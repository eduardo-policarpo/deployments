::Habilitar o WSL e a Virtual Machine Platform
wsl --install

::Instalar a distribuição Linux (Ubuntu, por padrão)
Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-2204 -OutFile ubuntu.appx
Rename-Item ubuntu.appx ubuntu.zip
Expand-Archive ubuntu.zip Ubuntu
Start-Process Ubuntu\ubuntu.exe

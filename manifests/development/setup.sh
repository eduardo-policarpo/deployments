#!/bin/bash

# *****************************************************************************
# Habilitar a Integração do Docker com WSL 2:
# *****************************************************************************
# 1. Abra o Docker Desktop.
# 2. Acesse Settings > General.
# 3. Certifique-se de que a opção 'Use the WSL 2 based engine' está marcada.
# 4. Vá para Settings > Resources > WSL Integration.
#    - Esta seção permite configurar quais distribuições Linux do WSL terão
#      integração com o Docker.
#
# 5. Ative a integração para a sua distribuição WSL específica usada para o desenvolvimento.
#    - Marque a caixa de seleção ao lado do nome da sua distribuição.
#
# 6. Clique em 'Apply & Restart'.
# *****************************************************************************

# Definição de cores para saída no terminal
readonly RED="\033[1;31m"
readonly WHITE="\033[1;37m"
readonly GREEN="\033[1;32m"
readonly GRAY_LIGHT="\033[0;37m"
readonly YELLOW="\033[1;33m"
readonly CYAN="\033[1;36m"
readonly NC="\033[0m" # Sem cor

# Definição de variáveis globais
export DOMAIN
export APP_URL
export API_URL
export USERS_URL
export MONGODB_ROOT_PASSWORD
export DIR_REPOS="$HOME/API-WHATSAPP"
export REPOS=("api-service" "users-service" "client-service" "worker")
export BASE_URL_REPO="https://gitlab.com/worker-group/api_whatsapp/"

# Verifica se o script está sendo executado como root
if [ "$EUID" -ne 0 ]; then
  printf "${YELLOW}⚠️ Este script deve ser executado como root. Use 'sudo ./setup.sh${NC}\n"
  exit 1
fi

# Função para determinar o sistema operacional e obter o endereço IP
get_ip_address() {
  case "$(uname -s)" in
  Linux*) ip=$(hostname -I | awk '{print $1}') ;;
  Darwin*) ip=$(ipconfig getifaddr en0) ;;
  *) ip="UNKNOWN:$(uname -s)" ;;
  esac
  echo $ip
}

# Função para adicionar o domínio e subdomínios ao arquivo /etc/hosts
add_to_hosts() {
  local ip=$(get_ip_address)
  local domain=$1
  local subdomains=("api" "users")
  local domains=("$domain")

  # Adicionar subdomínios à lista de domínios
  for sub in "${subdomains[@]}"; do
    domains+=("$sub.$domain")
  done

  # Adicionar domínios ao /etc/hosts
  for d in "${domains[@]}"; do
    local host_entry="$ip $d"
    if ! grep -q "$host_entry" /etc/hosts; then
      echo "$host_entry" | sudo tee -a /etc/hosts >/dev/null
      printf "${GREEN}✔️  Domínio $d adicionado ao /etc/hosts com IP $ip.${NC}\n"
    else
      printf "${YELLOW}⚠️  Domínio $d já existe no /etc/hosts.${NC}\n"
    fi
  done
}

# Função para instalar Docker
install_docker() {
  if ! command -v docker &>/dev/null; then
    printf "${CYAN}⚠️  Docker não encontrado. Instalando Docker...${NC}\n"
    if [[ "$(uname -s)" == "Darwin" ]]; then
      # Instalação do Docker para macOS usando Homebrew Cask
      brew install --cask docker

    elif [[ "$(uname -s)" == "Linux" ]]; then
      # Baixar e executar o script de instalação do Docker para Linux
      curl -fsSL https://get.docker.com -o get-docker.sh
      sudo sh get-docker.sh
    fi
    printf "${GREEN}✔️  Docker foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Docker já está instalado!${NC}\n"
  fi
}

install_node() {
  if command -v node &>/dev/null; then
    printf "${GREEN}✔️ Node.js já está instalado. Versão: $(node -v)${NC}\n"
  else
    printf "${CYAN}⚠️ Node.js não encontrado. Instalando Node.js...${NC}\n"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
    if nvm install 20; then
      printf "${GREEN}✔️ Node.js foi instalado com sucesso. Versão: $(node -v)${NC}\n"
    else
      printf "${RED}❌ Falha ao instalar o Node.js${NC}\n"
      return 1
    fi
  fi
}

# Função para instalar Kind
install_kind() {
  if ! command -v kind &>/dev/null; then
    printf "${CYAN}⚠️  Kind não encontrado. Instalando Kind...${NC}\n"

    # Determina o sistema operacional e a arquitetura para baixar a versão correta do Kind
    case "$(uname -s)" in
    Linux*)
      case "$(uname -m)" in
      x86_64) kind_url="https://kind.sigs.k8s.io/dl/v0.23.0/kind-linux-amd64" ;;
      aarch64) kind_url="https://kind.sigs.k8s.io/dl/v0.23.0/kind-linux-arm64" ;;
      *)
        echo "Arquitetura não suportada"
        exit 1
        ;;
      esac
      ;;
    Darwin*)
      case "$(uname -m)" in
      x86_64) kind_url="https://kind.sigs.k8s.io/dl/v0.23.0/kind-darwin-amd64" ;;
      arm64) kind_url="https://kind.sigs.k8s.io/dl/v0.23.0/kind-darwin-arm64" ;;
      *)
        echo "Arquitetura não suportada"
        exit 1
        ;;
      esac
      ;;
    *)
      echo "Sistema operacional não suportado"
      exit 1
      ;;
    esac

    # Baixa e instala Kind
    curl -Lo ./kind "$kind_url"
    chmod +x ./kind
    sudo mv ./kind /usr/local/bin/kind
    printf "${GREEN}✔️ Kind foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Kind já está instalado!.${NC}\n"
  fi
}

# Função para instalar Kubectl
install_kubectl() {
  if ! command -v kubectl &>/dev/null; then
    printf "${CYAN}⚠️  Kubectl não encontrado. Instalando Kubectl...${NC}\n"
    if [[ "$(uname -s)" == "Darwin" ]]; then
      brew install kubectl
    else
      sudo snap install kubectl --classic
    fi
    printf "${GREEN}✔️ Kubectl foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Kubectl já está instalado!${NC}\n"
  fi
}

# Função para instalar Helm
install_helm() {
  if ! command -v helm &>/dev/null; then
    printf "${CYAN}⚠️  Helm não encontrado. Instalando Helm...${NC}\n"
    if [[ "$(uname -s)" == "Darwin" ]]; then
      brew install helm
    else
      sudo snap install helm --classic
    fi
    printf "${GREEN}✔️ Helm foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Helm já está instalado!${NC}\n"
  fi
}

# Função para criar o cluster
create_kind_cluster() {
  if ! kind get clusters | grep -q api-server; then
    printf "${CYAN}⚠️ Criando o cluster api-server...${NC}\n"
    kind create cluster --name api-server --config - <<EOF
apiVersion: kind.x-k8s.io/v1alpha4
kind: Cluster
networking:
  apiServerAddress: $(get_ip_address)
  apiServerPort: 6443
nodes:
  - role: control-plane
    image: kindest/node:v1.29.0
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 80
        listenAddress: 0.0.0.0
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        listenAddress: 0.0.0.0
        protocol: TCP
  - role: worker
    image: kindest/node:v1.29.0
    extraMounts:
    - hostPath: "$DIR_REPOS"
      containerPath: /mnt
      propagation: None
      readOnly: false
  - role: worker
    image: kindest/node:v1.29.0
    extraMounts:
    - hostPath: "$DIR_REPOS"
      containerPath: /mnt
      propagation: None
      readOnly: false
EOF

    # habilitando metrics no kind
    kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml
    kubectl patch deployment metrics-server -n kube-system --patch "$(
      cat <<EOF
spec:
  template:
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls
        name: metrics-server
EOF
    )"
    printf "${GREEN}✔️ Cluster api-server criado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Cluster api-server já existe!.${NC}\n"
  fi
}

# Aguarde até que o cluster esteja pronto
wait_for_kind_cluster_ready() {
  printf "${CYAN}⚠️  Aguardando a implantação do cluster...${NC}\n"
  # Loop até que todos os nós estejam no estado "Ready"
  until kubectl get nodes | grep -q 'Ready'; do
    sleep 1
  done
  printf "${GREEN}✔️ Cluster status Ready!${NC}\n"
}

# Constrói e envia uma imagem Docker para o cluster
function build_and_push_docker_image() {
  # Cria um Dockerfile temporário
  sudo cat >Dockerfile <<-EOF
    FROM node:lts-alpine
    WORKDIR /usr/app
    RUN chmod -R 777 /usr/app
    RUN apk add --no-cache libc6-compat
EOF

  # Constrói a imagem Docker
  docker pull node:lts-alpine

  sudo docker build -t base:0.0.1 .

  # Carrega a imagem no cluster kind
  kind load docker-image base:0.0.1 --name api-server
}

while true; do
  printf "${WHITE}💻 Insira o domínio local para a API: Os domínios devem terminar com .local${GRAY_LIGHT} "
  read -r DOMAIN

  # Verifica se um valor foi fornecido
  if [ -z "$DOMAIN" ]; then
    printf "${YELLOW}❕ O domínio é obrigatório. Você deve informar um valor.❕ ${GRAY_LIGHT}\n"
    continue
  fi

  # Verifica se o domínio termina com .local
  if [[ $DOMAIN =~ \.local$ ]]; then
    printf "${GREEN}✔️ Domínio válido: $DOMAIN!${NC}\n"
    add_to_hosts $DOMAIN
    break
  else
    printf "${YELLOW}❕ O domínio deve terminar com .local. Por favor, tente novamente.❕ ${GRAY_LIGHT}\n"
  fi
done

# Atribuicao de valores as variaveis
APP_URL="http://${DOMAIN}"
API_URL="http://api.${DOMAIN}"
USERS_URL="http://users.${DOMAIN}"

while true; do
  printf "${WHITE}💻 Por favor, insira o diretório onde você deseja clonar os repositórios: (padrao: $DIR_REPOS)${GRAY_LIGHT} "
  read -r directory
  # Checa se um diretório foi informado, senão usa o padrão
  if [ -z "$directory" ]; then
    printf "${YELLOW}❕ Nenhum diretório foi informado. Utilizando o diretório padrão: $DIR_REPOS ❕${GRAY_LIGHT}\n"
    break
  else
    DIR_REPOS=$directory
    break
  fi
done

# Solicitar senha do MongoDB com validação e confirmação
while true; do
  printf "${WHITE}Digite a senha para o root do MongoDB (não pode ser vazia):${GRAY_LIGHT} "
  read -r -s mongodb_password
  printf "\n${WHITE}Confirme a senha:${GRAY_LIGHT} "
  read -r -s mongodb_password_confirm
  printf "\n"

  if [[ -z "$mongodb_password" || -z "$mongodb_password_confirm" ]]; then
    printf "${RED}Erro: A senha não pode ser vazia.${NC}\n"
  elif [[ "$mongodb_password" != "$mongodb_password_confirm" ]]; then
    printf "${RED}Erro: As senhas não coincidem.${NC}\n"
  else
    printf "${GREEN}Senha do MongoDB definida.${NC}\n"
    break
  fi
done

MONGODB_ROOT_PASSWORD=$mongodb_password

# Verifica se o sistema é Linux
if [[ "$(uname -s)" == "Linux" ]]; then
  # Atualiza a lista de pacotes
  echo "Atualizando lista de pacotes..."
  if ! sudo apt-get update; then
    echo "Erro ao atualizar a lista de pacotes." >&2
    exit 1
  fi

  # Instala o snapd
  echo "Instalando o snapd..."
  if ! sudo apt-get install -y snapd; then
    echo "Erro ao instalar o snapd." >&2
    exit 1
  fi

  # Verifica o status do serviço snapd
  echo "Verificando o status do serviço snapd..."
  if ! sudo systemctl status snapd >/dev/null 2>&1; then
    echo "O serviço snapd não está ativo. Tentando reiniciar..." >&2
    if ! sudo systemctl restart snapd; then
      echo "Falha ao reiniciar o serviço snapd." >&2
      exit 1
    fi
  fi

  # Verifica o status do snapd novamente
  if ! sudo systemctl is-active --quiet snapd; then
    echo "O serviço snapd ainda não está ativo após a tentativa de reinicialização." >&2
    exit 1
  fi

  echo "Instalação e configuração do snapd concluídas com sucesso."
fi

if [[ "$(uname -s)" == "Darwin" ]]; then
  if ! command -v envsubst &>/dev/null; then
    printf "${YELLOW}❕ envsubst não encontrado. Instalando... ${GRAY_LIGHT}\n"
    brew install gettext
  fi
fi

if ! command -v pnpm &>/dev/null; then
  echo "pnpm não encontrado. Instalando pnpm..."
  npm install -g pnpm
fi

# Verificar se o diretório existe, se não, criá-lo
mkdir -p "$DIR_REPOS"
cd "$DIR_REPOS" || exit


# Adicionar o diretório base como seguro
git config --global --add safe.directory "$DIR_REPOS"

# Clonar cada repositório e instalar dependências, apenas se não estiverem já clonados
for repo in "${REPOS[@]}"; do
  # Checa se o diretório do repositório já existe
  if [ -d "$DIR_REPOS/$repo" ]; then
    printf "${YELLOW}⚠️  O repositório $repo já existe. Pulando clonagem...${NC}\n"
    # Apenas vai para o diretório e instala dependências
    cd "$DIR_REPOS/$repo"
    printf "${CYAN}⚠️  Atualizando dependências para $repo...${NC}\n"
    pnpm install
    cd "$DIR_REPOS"  # Volta para o diretório pai após a instalação
  else
    # O diretório não existe, clona e instala dependências
    printf "${CYAN}⚠️  Clonando repositório $repo...${NC}\n"
    git clone "${BASE_URL_REPO}${repo}.git"
    if [ -d "$repo" ]; then
      printf "${CYAN}⚠️  Instalando dependências para $repo...${NC}\n"
      cd "$repo"
      pnpm install
      cd ..
    else
      printf "${RED}❕ Erro ao clonar $repo. Verifique as configurações do repositório.${GRAY_LIGHT}\n"
    fi
  fi
done
printf "${GREEN}✔️  Todos os repositórios foram clonados ou atualizados e as dependências instaladas.${NC}\n"

sudo  mkdir -p ${DIR_REPOS}/api-data  
sudo chmod -R 777 $DIR_REPOS


install_node
install_docker
install_kind
install_kubectl
install_helm
create_kind_cluster
wait_for_kind_cluster_ready
build_and_push_docker_image

helm repo add jetstack https://charts.jetstack.io
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# Instala o Prometheus
helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace

# deploy rabbitmq
helm install rabbitmq-cluster bitnami/rabbitmq \
  --set communityPlugins="https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.12.0/rabbitmq_delayed_message_exchange-3.12.0.ez" \
  --set extraPlugins="rabbitmq_delayed_message_exchange" \
  --set replicaCount=1 \
  --set auth.username=admin \
  --set auth.password=admin \
  --set auth.securePassword=false \
  --set persistence.enabled=true \
  --set persistence.size=5Gi \
  --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
  --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
  --set service.externalIPs[0]=$(get_ip_address) \
  --set service.type=NodePort \
  --set service.nodePorts.amqp=30072 \
  --set service.nodePorts.management=30073 \
  --namespace infra --create-namespace

# Aguardar os pods do RabbitMQ  estarem prontos
printf "${YELLOW}⚠️  Aguardando RabbitNQM ficar com Status Ready...${NC}\n"
kubectl rollout status statefulset/rabbitmq-cluster -n infra --timeout=600s


# deploy redis
helm install redis-server bitnami/redis \
  --set master.persistence.size=2Gi \
  --set architecture="replication" \
  --set replicaCount=1 \
  --set master.service.type=NodePort \
  --set master.service.nodePorts.redis=30007 \
  --set master.service.externalIPs[0]=$(get_ip_address) \
  --set auth.enabled=false \
  --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
  --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
  --namespace infra --create-namespace

# Aguardar os pods do Redis  estarem prontos
printf "${YELLOW}⚠️  Aguardando Redis ficar com Status Ready...${NC}\n"
kubectl rollout status statefulset/redis-server-master -n infra --timeout=600s
kubectl rollout status statefulset/redis-server-replicas -n infra --timeout=600s


# deploy mongodb
helm install mongodb-standalone bitnami/mongodb \
  --set image.repository=bitnami/mongodb \
  --set image.tag=7.0.11 \
  --set image.pullPolicy=Always \
  --set architecture="standalone" \
  --set auth.enabled=true \
  --set auth.rootPassword=$MONGODB_ROOT_PASSWORD \
  --set auth.usernames={admin} \
  --set auth.passwords={admin} \
  --set auth.databases={wappi} \
  --set externalAccess.autoDiscovery.resourcesPreset="medium" \
  --set service.type=NodePort \
  --set service.externalIPs[0]=$(get_ip_address) \
  --set service.nodePorts.mongodb=30676 \
  --set externalAccess.service.nodePorts[0]=30676 \
  --set externalAccess.service.type=NodePort \
  --namespace infra --create-namespace

# Aguardar os pods do MongoDB estarem prontos
printf "${YELLOW}⚠️  Aguardando MongoDB ficar com Status Ready...${NC}\n"
kubectl rollout status deployment/mongodb-standalone -n infra --timeout=600s


# Aplicar o job para configurar o MongoDB
printf "${GREEN}✔️ Aplicando o job de configuração do MongoDB...${NC}\n"
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/mongodb/mongodb-setup-job.yaml?ref_type=heads
printf "${YELLOW}⚠️  Verificando o status do job de configuração do MongoDB...${NC}\n"
kubectl wait --for=condition=complete job/mongodb-setup -n infra --timeout=300s

# aplicar secrets
printf "${GREEN}✔️ Aplicando as secrets no cluster...${NC}\n"
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/gitlab-registry.yaml?ref_type=heads
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/jwt-secret.yaml?ref_type=heads

# deploy client-service
printf "${GREEN}✔️ Deploy e configuracao da aplicacao client-service...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/client-service-dev.yaml?ref_type=heads >client-service-dev.yaml
envsubst <client-service-dev.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=client-service --timeout=300s

# deploy api-server
printf "${GREEN}✔️ Deploy e configuracao da aplicacao api-server...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/api-service-dev.yaml?ref_type=heads >api-service-dev.yaml
export APP_URL="${API_URL}"
envsubst <api-service-dev.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=api-service --timeout=300s

# deploy user-service
printf "${GREEN}✔️ Deploy e configuracao da aplicacao user-service...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/user-service-dev.yaml?ref_type=heads >user-service-dev.yaml
export APP_URL="${USERS_URL}"
envsubst <user-service-dev.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=user-service --timeout=300s

# deploy ingress controller
printf "${GREEN}✔️ Deploy e configuracao do ingress controller...${NC}\n"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=180s

# Esperar pelo webhook de admissão ficar pronto
while ! kubectl get --raw "/apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations/ingress-nginx-admission" &>/dev/null; do
  printf "${YELLOW}⚠️  Aguardando o webhook de admissão do Ingress ficar com Status Ready...${NC}\n"
  sleep 10
done

# Criar Ingress
cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
spec:
  ingressClassName: nginx
  rules:
  - host: ${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: client-service-svc
            port:
              number: 3000
  - host: api.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: api-service-svc
            port:
              number: 3000
  - host: users.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: user-service-svc
            port:
              number: 3000
EOF

kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: api-data
provisioner: kubernetes.io/no-provisioner
reclaimPolicy: Retain
volumeBindingMode: WaitForFirstConsumer

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: api-storage
spec:
  storageClassName: api-data
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: api-data-pv
spec:
  storageClassName: api-data
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/api-data"
EOF

# export KUBECONFIG
kubectl config view --raw >kubeConfig.yaml
OS=$(uname)
if [ "$OS" = "Darwin" ]; then
  # macOS
sed -i '' -e "s|server: https://.*:16443|server: https://${DOMAIN}:16443|" kubeConfig.yaml
sed -i '' -e '/certificate-authority-data/d' kubeConfig.yaml
sed -i '' -e '/server: https:\/\//a \
\ \ \ \ insecure-skip-tls-verify: true' kubeConfig.yaml
elif [ "$OS" = "Linux" ]; then
  # Linux
  sed -i -e "s|server: https://.*:16443|server: https://${DOMAIN}:16443|" kubeConfig.yaml
  sed -i -e '/certificate-authority-data/d' kubeConfig.yaml
  sed -i -e '/server: https:\/\//a \ \ \ \ insecure-skip-tls-verify: true' kubeConfig.yaml
else
  exit 1
  echo "Sistema operacional não suportado: $OS"
fi


kind get kubeconfig --name api-server >~/.kube/config

printf "${GREEN} INSTALACAO CONCLUIDA 🚀🚀🚀🚀${GRAY_LIGHT} "

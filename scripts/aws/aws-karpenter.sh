



# #!/bin/bash

# # Definindo variáveis de ambiente
# export K8S_VERSION="1.29"
# export CLUSTER_NAME="api-cluster"
# export AWS_DEFAULT_REGION="sa-east-1"
# export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"

# # Verificando e criando o cluster EKS
# if ! eksctl get cluster --name ${CLUSTER_NAME} --region ${AWS_DEFAULT_REGION} >/dev/null 2>&1; then
#     echo "Criando o cluster ${CLUSTER_NAME}..."
#     eksctl create cluster \
#         --name ${CLUSTER_NAME} \
#         --version ${K8S_VERSION} \
#         --region ${AWS_DEFAULT_REGION} \
#         --nodegroup-name node-autoscaling \
#         --node-type t3.large \
#         --nodes-min 1 \
#         --nodes-max 10 \
#         --asg-access
# fi

# # Associando provedor IAM OIDC
# echo "Associando provedor IAM OIDC..."
# eksctl utils associate-iam-oidc-provider --region ${AWS_DEFAULT_REGION} --cluster ${CLUSTER_NAME} --approve

# # Preparando a política IAM para o Cluster Autoscaler
# export AUTOSCALER_POLICY_FILE="/tmp/cluster-autoscaler-policy.json"
# cat <<EOF > ${AUTOSCALER_POLICY_FILE}
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "autoscaling:DescribeAutoScalingGroups",
#                 "autoscaling:DescribeAutoScalingInstances",
#                 "autoscaling:DescribeLaunchConfigurations",
#                 "autoscaling:DescribeTags",
#                 "autoscaling:SetDesiredCapacity",
#                 "autoscaling:TerminateInstanceInAutoScalingGroup",
#                 "ec2:DescribeLaunchTemplateVersions"
#             ],
#             "Resource": "*"
#         }
#     ]
# }
# EOF

# # Criando a política IAM se ela não existir
# POLICY_ARN=$(aws iam list-policies --query "Policies[?PolicyName=='ClusterAutoscalerPolicy'].Arn" --output text)
# if [ -z "$POLICY_ARN" ]; then
#     echo "Criando política IAM para Cluster Autoscaler..."
#     POLICY_ARN=$(aws iam create-policy \
#         --policy-name ClusterAutoscalerPolicy \
#         --policy-document file://${AUTOSCALER_POLICY_FILE} \
#         --query 'Policy.Arn' \
#         --output text)
# else
#     echo "Política IAM para Cluster Autoscaler já existe."
# fi

# # Criando uma service account para o Cluster Autoscaler com a política IAM
# echo "Criando service account para Cluster Autoscaler..."
# eksctl create iamserviceaccount \
#     --region ${AWS_DEFAULT_REGION} \
#     --name cluster-autoscaler \
#     --namespace kube-system \
#     --cluster ${CLUSTER_NAME} \
#     --attach-policy-arn ${POLICY_ARN} \
#     --override-existing-serviceaccounts \
#     --approve

# # Adicionando o repositório Helm para o Cluster Autoscaler e atualizando
# echo "Configurando repositório Helm para Cluster Autoscaler..."
# helm repo add autoscaler https://kubernetes.github.io/autoscaler
# helm repo update

# # Instalando o Cluster Autoscaler
# echo "Instalando o Cluster Autoscaler..."
# helm install cluster-autoscaler autoscaler/cluster-autoscaler \
#     --namespace kube-system \
#     --set autoDiscovery.clusterName=${CLUSTER_NAME} \
#     --set awsRegion=${AWS_DEFAULT_REGION}


# # Instalando o CSI driver para o EBS
# echo "Instalando o CSI driver para o EBS..."
# eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
# eksctl create iamserviceaccount \
#   --name ebs-csi-controller-sa \
#   --namespace kube-system \
#   --cluster ${CLUSTER_NAME} \
#   --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
#   --approve \
#   --role-only \
#   --role-name AmazonEKS_EBS_CSI_DriverRole
# eksctl create addon --name aws-ebs-csi-driver --cluster ${CLUSTER_NAME} --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole --force


# # Instalando mongoDB
# echo "Instalando mongoDB..."
# helm repo add mongodb https://mongodb.github.io/helm-charts
# helm repo update
# kubectl create namespace mongodb
# kubectl label namespace mongodb monitoring=prometheus
# helm install mongodb-community-operator mongodb/mongodb-community-operator --namespace mongodb
# kubectl create secret generic mongodb-secret --from-literal=password='admin123' -n mongodb


# kubectl apply -f - <<EOF
# ---
# apiVersion: mongodbcommunity.mongodb.com/v1
# kind: MongoDBCommunity
# metadata:
#   name: mongodb-replica-set
#   namespace: mongodb
# spec:
#   members: 1
#   type: ReplicaSet
#   version: "6.0.5"
#   security:
#     tls:
#       enabled: true
#       certificateKeySecretRef:
#         name: mongodb-key-pair
#     authentication:
#       modes: ["SCRAM"]
#   users:
#   - name: dbuser
#     db: admin
#     passwordSecretRef:
#       name: mongodb-secret
#     roles:
#     - name: clusterAdmin
#       db: admin
#     - name: userAdminAnyDatabase
#       db: admin
#     scramCredentialsSecretName: my-scram
#   additionalMongodConfig:
#     storage.wiredTiger.engineConfig.journalCompressor: zlib
#   statefulSet:
#     spec:
#       template:
#         spec:
#           containers:
#           - name: mongod
#             resources:
#               limits:
#                 cpu: "1"
#                 memory: 2Gi
#               requests:
#                 cpu: 500m
#                 memory: 1Gi      
#           affinity:
#             podAntiAffinity:
#               requiredDuringSchedulingIgnoredDuringExecution:
#               - labelSelector:
#                   matchExpressions:
#                   - key: app
#                     operator: In
#                     values:
#                     - my-mongodb
#                 topologyKey: "kubernetes.io/hostname"
#       volumeClaimTemplates:
#       - metadata:
#           name: data-volume
#         spec:
#           accessModes:
#           - ReadWriteOnce
#           resources:
#             requests:
#               storage: 40G
# ---
# apiVersion: v1
# kind: Secret
# metadata:
#   name: mongodb-secret
# type: Opaque
# stringData:
#   password: admin123
# EOF

# echo "Script concluído."






#!/bin/bash

# Definindo variáveis de ambiente
export K8S_VERSION="1.29"
export CLUSTER_NAME="api-whatsapp"
export AWS_DEFAULT_REGION="sa-east-1"
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"

# Verificando e criando o cluster EKS
if ! eksctl get cluster --name ${CLUSTER_NAME} --region ${AWS_DEFAULT_REGION} >/dev/null 2>&1; then
    echo "Criando o cluster ${CLUSTER_NAME}..."
    eksctl create cluster \
        --name ${CLUSTER_NAME} \
        --version ${K8S_VERSION} \
        --region ${AWS_DEFAULT_REGION} \
        --nodegroup-name node-autoscaling \
        --node-type t3.large \
        --nodes-min 1 \
        --nodes-max 10 \
        --asg-access
fi

# Associando provedor IAM OIDC
echo "Associando provedor IAM OIDC..."
eksctl utils associate-iam-oidc-provider --region ${AWS_DEFAULT_REGION} --cluster ${CLUSTER_NAME} --approve

# Preparando a política IAM para o Cluster Autoscaler
export AUTOSCALER_POLICY_FILE="/tmp/cluster-autoscaler-policy.json"
cat <<EOF > ${AUTOSCALER_POLICY_FILE}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions"
            ],
            "Resource": "*"
        }
    ]
}
EOF

# Criando a política IAM se ela não existir
POLICY_ARN=$(aws iam list-policies --query "Policies[?PolicyName=='ClusterAutoscalerPolicy'].Arn" --output text)
if [ -z "$POLICY_ARN" ]; then
    echo "Criando política IAM para Cluster Autoscaler..."
    POLICY_ARN=$(aws iam create-policy \
        --policy-name ClusterAutoscalerPolicy \
        --policy-document file://cluster-autoscaler-policy.json \
        --query 'Policy.Arn' \
        --output text)
else
    echo "Política IAM para Cluster Autoscaler já existe."
fi

# Criando uma service account para o Cluster Autoscaler com a política IAM
echo "Criando service account para Cluster Autoscaler..."
eksctl create iamserviceaccount \
    --region ${AWS_DEFAULT_REGION} \
    --name cluster-autoscaler \
    --namespace kube-system \
    --cluster ${CLUSTER_NAME} \
    --attach-policy-arn ${POLICY_ARN} \
    --override-existing-serviceaccounts \
    --approve

# Configurando repositório Helm para o Cluster Autoscaler e atualizando
echo "Configurando repositório Helm para Cluster Autoscaler..."
helm repo add autoscaler https://kubernetes.github.io/autoscaler
helm repo update

# Instalando o Cluster Autoscaler
echo "Instalando o Cluster Autoscaler..."
helm install cluster-autoscaler autoscaler/cluster-autoscaler \
    --namespace kube-system \
    --set autoDiscovery.clusterName=${CLUSTER_NAME} \
    --set awsRegion=${AWS_DEFAULT_REGION}

# Instalando o CSI driver para o EBS
eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
eksctl create iamserviceaccount \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster ${CLUSTER_NAME} \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole
eksctl create addon --name aws-ebs-csi-driver --cluster ${CLUSTER_NAME} --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole --force

# Instalando mongoDB
echo "Instalando mongoDB..."
helm repo add mongodb https://mongodb.github.io/helm-charts
helm repo update
kubectl create namespace mongodb
kubectl label namespace mongodb monitoring=prometheus

# Instalando o MongoDB Community Operator
helm install community-operator mongodb/community-operator --namespace mongodb
kubectl wait --for=condition=available --timeout=600s deployment/mongodb-kubernetes-operator --namespace mongodb


# Cria o MongoDBCommunity e a Secret necessária
echo "Aplicando manifestos do MongoDB..."
kubectl apply -f - <<EOF
apiVersion: mongodbcommunity.mongodb.com/v1
kind: MongoDBCommunity
metadata:
  name: mongodb-replica-set
  namespace: mongodb
spec:
  members: 3
  type: ReplicaSet
  version: "6.0.5"
  security:
    authentication:
      modes: ["SCRAM"]
  users:
  - name: admin
    db: admin
    passwordSecretRef:
      name: mongodb-secret
    roles:
      - name: root
        db: admin
    scramCredentialsSecretName: my-scram
  additionalMongodConfig:
    storage.wiredTiger.engineConfig.journalCompressor: zlib
  statefulSet:
    spec:
      template:
        spec:
          containers:
            - name: mongod
              resources:
                limits:
                  cpu: "1"
                  memory: 2Gi
                requests:
                  cpu: 500m
                  memory: 1Gi
          affinity:
            podAntiAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
                - labelSelector:
                    matchExpressions:
                      - key: app
                        operator: In
                        values:
                          - mongodb-replica-set
                  topologyKey: "kubernetes.io/hostname"
      volumeClaimTemplates:
        - metadata:
            name: data-volume
          spec:
            accessModes:
              - ReadWriteOnce
            resources:
              requests:
                storage: 10Gi
---
apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
  namespace: mongodb
type: Opaque
stringData:
  password: admin123
EOF

while [[ $(kubectl get MongoDBCommunity mongodb-replica-set -n mongodb -o 'jsonpath={..status.phase}') != "Running" ]]; do
  echo "Waiting for MongoDB replica set to be ready..."
  sleep 10
done


echo "Script concluído."

#!/bin/bash

# Nome do cluster para deletar
CLUSTER_NAME=$1
# Região do cluster
REGION=$2

# Deleta o cluster com eksctl
eksctl delete cluster --name "$CLUSTER_NAME" --region "$REGION"

# Encontra o contexto do cluster no kubeconfig
CONTEXT=$(kubectl config get-contexts -o name | grep "$CLUSTER_NAME")

# Se um contexto foi encontrado, procede com a remoção
if [ ! -z "$CONTEXT" ]; then
    echo "Removendo contexto do cluster: $CONTEXT"
    kubectl config delete-context "$CONTEXT"
    
    echo "Removendo configurações do cluster: $CLUSTER_NAME"
    kubectl config delete-cluster "$CLUSTER_NAME"
    
    # Opcional: Remover o usuário associado, se desejado. Você precisaria determinar o nome do usuário (user name).
fi

# Define outro contexto como padrão, se necessário
# kubectl config use-context outro-contexto

#!/bin/bash

# # Configuração inicial
# export KARPENTER_VERSION="0.35.1"
# export K8S_VERSION="1.29" # Versão compatível com o Karpenter e suportada pelo EKS
# export CLUSTER_NAME="api-cluster"
# export AWS_DEFAULT_REGION="sa-east-1"
# export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
# export IAM_ROLE_NAME="KarpenterNodeRole-${CLUSTER_NAME}"

# # Criação do cluster EKS com eksctl
# eksctl create cluster \
#   --name ${CLUSTER_NAME} \
#   --version ${K8S_VERSION} \
#   --region ${AWS_DEFAULT_REGION} \
#   --nodegroup-name ${CLUSTER_NAME}-ng \
#   --nodes 2 \
#   --nodes-min 1 \
#   --nodes-max 10 \
#   --with-oidc \
#   --ssh-access \
#   --ssh-public-key my-public-key.pem \
#   --managed

# # Criação da política IAM para o Karpenter
# POLICY_ARN=$(aws iam create-policy \
#   --policy-name KarpenterControllerPolicy-${CLUSTER_NAME} \
#   --policy-document file://karpenter-controller-policy.json \
#   --query Policy.Arn \
#   --output text)

# # Criação do role do IAM para o Karpenter
# ROLE_ARN=$(aws iam create-role \
#   --role-name ${IAM_ROLE_NAME} \
#   --assume-role-policy-document file://trust-relationship.json \
#   --description "Karpenter node role for ${CLUSTER_NAME}" \
#   --query Role.Arn \
#   --output text)

# # Associação da política ao role
# aws iam attach-role-policy --role-name ${IAM_ROLE_NAME} --policy-arn ${POLICY_ARN}

# # Atualização do aws-auth ConfigMap
# kubectl get configmap/aws-auth -n kube-system -o yaml > aws-auth.yaml
# echo "    - rolearn: ${ROLE_ARN}" >> aws-auth.yaml
# echo "      username: system:node:{{EC2PrivateDNSName}}" >> aws-auth.yaml
# echo "      groups:" >> aws-auth.yaml
# echo "        - system:bootstrappers" >> aws-auth.yaml
# echo "        - system:nodes" >> aws-auth.yaml
# kubectl apply -f aws-auth.yaml

# # Adição do repositório Helm do Karpenter e instalação
# helm repo add karpenter https://charts.karpenter.sh/
# helm repo update

# helm upgrade --install karpenter karpenter/karpenter \
#   --namespace kube-system \
#   --version ${KARPENTER_VERSION} \
#   --set serviceAccount.create=true \
#   --set serviceAccount.name=karpenter \
#   --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"=${ROLE_ARN}

# # Aplicar um Provisioner padrão
# kubectl apply -f provisioner.yaml

# echo "Cluster ${CLUSTER_NAME} criado e configurado com sucesso!"

# /*
# Vamos refatorar e corrigir seu script para garantir que ele siga as melhores práticas e esteja atualizado conforme a documentação mais recente do Karpenter e do EKS. A seguir, apresento uma versão corrigida e otimizada do script, com algumas suposições e melhorias:

# 1. Certificaremos que todas as políticas e roles do IAM necessárias sejam criadas e associadas corretamente.
# 2. Incluiremos a instalação do Karpenter via Helm.
# 3. Removeremos configurações redundantes ou desnecessárias.
# 4. Simplificaremos o processo de atualização do `aws-auth` ConfigMap.

# ```bash
# #!/bin/bash

# # Configuração inicial
# export KARPENTER_VERSION="0.35.1"
# export K8S_VERSION="1.21" # Versão compatível com o Karpenter e suportada pelo EKS
# export CLUSTER_NAME="api-cluster"
# export AWS_DEFAULT_REGION="sa-east-1"
# export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
# export IAM_ROLE_NAME="KarpenterNodeRole-${CLUSTER_NAME}"

# # Criação do cluster EKS com eksctl
# eksctl create cluster \
#   --name ${CLUSTER_NAME} \
#   --version ${K8S_VERSION} \
#   --region ${AWS_DEFAULT_REGION} \
#   --nodegroup-name ${CLUSTER_NAME}-ng \
#   --nodes 2 \
#   --nodes-min 1 \
#   --nodes-max 10 \
#   --with-oidc \
#   --ssh-access \
#   --ssh-public-key my-public-key.pem \
#   --managed

# # Criação da política IAM para o Karpenter
# POLICY_ARN=$(aws iam create-policy \
#   --policy-name KarpenterControllerPolicy-${CLUSTER_NAME} \
#   --policy-document file://karpenter-controller-policy.json \
#   --query Policy.Arn \
#   --output text)

# # Criação do role do IAM para o Karpenter
# ROLE_ARN=$(aws iam create-role \
#   --role-name ${IAM_ROLE_NAME} \
#   --assume-role-policy-document file://trust-relationship.json \
#   --description "Karpenter node role for ${CLUSTER_NAME}" \
#   --query Role.Arn \
#   --output text)

# # Associação da política ao role
# aws iam attach-role-policy --role-name ${IAM_ROLE_NAME} --policy-arn ${POLICY_ARN}

# # Atualização do aws-auth ConfigMap
# kubectl get configmap/aws-auth -n kube-system -o yaml > aws-auth.yaml
# echo "    - rolearn: ${ROLE_ARN}" >> aws-auth.yaml
# echo "      username: system:node:{{EC2PrivateDNSName}}" >> aws-auth.yaml
# echo "      groups:" >> aws-auth.yaml
# echo "        - system:bootstrappers" >> aws-auth.yaml
# echo "        - system:nodes" >> aws-auth.yaml
# kubectl apply -f aws-auth.yaml

# # Adição do repositório Helm do Karpenter e instalação
# helm repo add karpenter https://charts.karpenter.sh/
# helm repo update

# helm upgrade --install karpenter karpenter/karpenter \
#   --namespace kube-system \
#   --version ${KARPENTER_VERSION} \
#   --set serviceAccount.create=true \
#   --set serviceAccount.name=karpenter \
#   --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"=${ROLE_ARN}

# # Aplicar um Provisioner padrão
# kubectl apply -f provisioner.yaml

# echo "Cluster ${CLUSTER_NAME} criado e configurado com sucesso!"
# ```
# /*
# Notas Importantes:

# - Você precisa criar o arquivo `karpenter-controller-policy.json` com as permissões adequadas para o Karpenter. A documentação oficial do Karpenter fornece um exemplo que pode ser utilizado.
# - O arquivo `trust-relationship.json` deve permitir que o serviço `eks.amazonaws.com` e `ec2.amazonaws.com` assumam o role, conforme necessário para operações do Karpenter e do EKS.
# - O arquivo `provisioner.yaml` deve ser ajustado de acordo com suas necessidades. Este arquivo define como o Karpenter deve provisionar os recursos.
# - Certifique-se de ter `eksctl`, `kubectl`, `aws cli`, e `helm` instalados e configurados corretamente em sua máquina antes de executar este script.
# - Substitua `my-public-key.pem` pelo caminho da sua chave pública SSH, caso queira acesso SSH aos nós do cluster.
# - Este script assume que você está rodando em um contexto onde tem permissões adequadas na AWS para criar recursos.
# */

export KARPENTER_NAMESPACE="kube-system"
export KARPENTER_VERSION="0.35.2"
export K8S_VERSION="1.29"
export AWS_PARTITION="aws"
export CLUSTER_NAME="api-smille"
export AWS_DEFAULT_REGION="sa-east-1"
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
export USER_ARN="$(aws sts get-caller-identity --query Arn --output text)"
export IAM_ROLE_NAME="KarpenterNodeRole-${CLUSTER_NAME}"
export TEMPOUT="$(mktemp)"

curl -fsSL https://raw.githubusercontent.com/aws/karpenter-provider-aws/v"${KARPENTER_VERSION}"/website/content/en/preview/getting-started/getting-started-with-karpenter/cloudformation.yaml >"${TEMPOUT}" &&
  aws cloudformation deploy \
    --stack-name "Karpenter-${CLUSTER_NAME}" \
    --template-file "${TEMPOUT}" \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameter-overrides "ClusterName=${CLUSTER_NAME}"

eksctl create cluster -f - <<EOF
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig
metadata:
  name: ${CLUSTER_NAME}
  region: ${AWS_DEFAULT_REGION}
  version: "${K8S_VERSION}"
  tags:
    karpenter.sh/discovery: ${CLUSTER_NAME}

iam:
  withOIDC: true
  podIdentityAssociations:
  - namespace: "${KARPENTER_NAMESPACE}"
    serviceAccountName: karpenter
    roleName: ${CLUSTER_NAME}-karpenter
    permissionPolicyARNs:
    - arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:policy/KarpenterControllerPolicy-${CLUSTER_NAME}

iamIdentityMappings:
- arn: "arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/KarpenterNodeRole-${CLUSTER_NAME}"
  username: system:node:{{EC2PrivateDNSName}}
  groups:
  - system:bootstrappers
  - system:nodes

managedNodeGroups:
- instanceType: m5.large
  amiFamily: AmazonLinux2
  name: ${CLUSTER_NAME}-ng
  desiredCapacity: 2
  minSize: 1
  maxSize: 10

addons:
- name: eks-pod-identity-agent
EOF

export CLUSTER_ENDPOINT="$(aws eks describe-cluster --name "${CLUSTER_NAME}" --query "cluster.endpoint" --output text)"
export KARPENTER_IAM_ROLE_ARN="arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/${CLUSTER_NAME}-karpenter"

echo "${CLUSTER_ENDPOINT} ${KARPENTER_IAM_ROLE_ARN}"

export TRUST_RELATIONSHIP_FILE="trust-relationship.json"
cat <<EOF >${TRUST_RELATIONSHIP_FILE}
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

# 1. Criar a função de IAM
aws iam create-role \
  --role-name ${IAM_ROLE_NAME} \
  --assume-role-policy-document file://${TRUST_RELATIONSHIP_FILE} \
  --description "Função IAM para acesso ao cluster EKS"
rm trust-relationship.json

# 1. Associar política ao usuário ou função de IAM
aws iam attach-role-policy --role-name $IAM_ROLE_NAME --policy-arn arn:aws:iam::aws:policy/AmazonEKSClusterPolicy

# 2. Atualizar o aws-auth ConfigMap para adicionar o usuário ou função de IAM
EKSCTL_ROLE_ARN=$(aws iam list-roles \
  --query 'Roles[?contains(RoleName, `eksctl-api-smille-nodegroup-api-sm-NodeInstanceRole`)].Arn' \
  --output text)

kubectl apply -f - <<EOF
apiVersion: v1
data:
  mapRoles: |
    - groups:
      - system:bootstrappers
      - system:nodes
      rolearn: arn:aws:iam::${AWS_ACCOUNT_ID}:role/KarpenterNodeRole-${CLUSTER_NAME}
      username: system:node:{{EC2PrivateDNSName}}
    - groups:
      - system:bootstrappers
      - system:nodes
      rolearn: ${EKSCTL_ROLE_ARN}
      username: system:node:{{EC2PrivateDNSName}}
    - groups:
      - system:masters
      rolearn: ${USER_ARN}
  mapUsers: |
    []
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
EOF

# Logout of helm registry to perform an unauthenticated pull against the public ECR
helm registry logout public.ecr.aws

helm upgrade --install karpenter oci://public.ecr.aws/karpenter/karpenter \
  --version "${KARPENTER_VERSION}" \
  --namespace "${KARPENTER_NAMESPACE}" --create-namespace \
  --set "settings.clusterName=${CLUSTER_NAME}" \
  --set "settings.interruptionQueue=${CLUSTER_NAME}" \
  --set serviceAccount.create=true \
  --set clusterEndpoint=${CLUSTER_ENDPOINT} \
  --set aws.defaultInstanceProfile=KarpenterNodeInstanceProfile-${CLUSTER_NAME} \
  --wait

cat <<EOF | envsubst | kubectl apply -f -
apiVersion: karpenter.sh/v1beta1
kind: NodePool
metadata:
  name: default
spec:
  template:
    spec:
      requirements:
        - key: kubernetes.io/arch
          operator: In
          values: ["amd64"]
        - key: kubernetes.io/os
          operator: In
          values: ["linux"]
        - key: karpenter.sh/capacity-type
          operator: In
          values: ["spot"]
        - key: karpenter.k8s.aws/instance-category
          operator: In
          values: ["c", "m", "r"]
        - key: karpenter.k8s.aws/instance-generation
          operator: Gt
          values: ["2"]
      nodeClassRef:
        apiVersion: karpenter.k8s.aws/v1beta1
        kind: EC2NodeClass
        name: default
  limits:
    cpu: 1000
  disruption:
    consolidationPolicy: WhenUnderutilized
    expireAfter: 720h # 30 * 24h = 720h
---
apiVersion: karpenter.k8s.aws/v1beta1
kind: EC2NodeClass
metadata:
  name: default
spec:
  amiFamily: AL2 # Amazon Linux 2
  role: "KarpenterNodeRole-${CLUSTER_NAME}" # replace with your cluster name
  subnetSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}" # replace with your cluster name
  securityGroupSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}" # replace with your cluster name
  blockDeviceMappings:
  - deviceName: /dev/xvda
    ebs:
      volumeType: gp3
      volumeSize: 100Gi
      deleteOnTermination: true
EOF

helm upgrade --install karpenter-crd oci://public.ecr.aws/karpenter/karpenter-crd --version ${KARPENTER_VERSION} --namespace kube-system

#!/bin/bash

# Função para verificar se uma ferramenta está instalada
check_tool_installed() {
  if ! command -v $1 &>/dev/null; then
    echo "$1 não está instalado."
    return 1
  else
    echo "$1 já está instalado."
    return 0
  fi
}

# Função para verificar e configurar o AWS CLI
check_configure_aws_cli() {
  check_tool_installed aws
  if [ $? -eq 1 ]; then
    echo "Instalando o AWS CLI..."
    if [[ "$OSTYPE" == "darwin"* ]]; then
      # macOS
      curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
      sudo installer -pkg AWSCLIV2.pkg -target /
    elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
      # Linux
      curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
      unzip awscliv2.zip
      sudo ./aws/install
    else
      echo "Sistema operacional não suportado."
      exit 1
    fi

    echo "AWS CLI instalado com sucesso."
    echo "Por favor, crie sua ACCESS KEY e SECRET KEY acessando o serviço IAM em: https://console.aws.amazon.com/iam/"
    echo "Após ter os dados, execute o comando 'aws configure' no terminal e preencha os dados."
    echo "Rode este script novamente após configurar as credenciais."
    exit 0
  else
    echo "Verificando se as credenciais da AWS estão configuradas..."
    if ! aws sts get-caller-identity &>/dev/null; then
      echo "Credenciais da AWS não configuradas ou inválidas."
      echo "Por favor, configure suas credenciais da AWS executando 'aws configure'."
      exit 1
    else
      echo "Credenciais da AWS verificadas com sucesso."
    fi
  fi
}

# Verifica e configura o AWS CLI
check_configure_aws_cli

# Função para verificar se uma ferramenta está instalada
check_tool() {
  if ! command -v $1 &>/dev/null; then
    echo "$1 não está instalado."
    return 1
  else
    echo "$1 já está instalado."
    return 0
  fi
}

# Verificar e instalar aws-cli
check_tool aws || install_aws_cli

# Verificar e instalar helm
check_tool helm || {
  echo "Instalando Helm..."
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
}

# Verificar e instalar kubectl
check_tool kubectl || {
  echo "Instalando kubectl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install kubectl || brew link --overwrite kubernetes-cli
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
  fi
}

# Verificar e instalar eksctl
check_tool eksctl || {
  echo "Instalando eksctl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew tap weaveworks/tap
    brew install weaveworks/tap/eksctl
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
    sudo mv /tmp/eksctl /usr/local/bin
  fi
}

# Continuação do script após a verificação das ferramentas
echo "Todas as ferramentas necessárias estão instaladas."
echo "Procedendo com a configuração do cluster..."

export KARPENTER_NAMESPACE="kube-system"
export KARPENTER_VERSION="0.35.1"
export K8S_VERSION="1.29"
export DOMAIN_NAME="zap.dev.br"
export AWS_PARTITION="aws"
export CLUSTER_NAME="api-cluster"
export AWS_DEFAULT_REGION="sa-east-1"
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
export USER_ARN="$(aws sts get-caller-identity --query Arn --output text)"
export TEMPOUT=$(mktemp)
export IAM_ROLE_NAME="EKSAccessRole"
export ROLE_ARN="arn:aws:iam::${AWS_ACCOUNT_ID}:role/${IAM_ROLE_NAME}"

eksctl create cluster -f - <<EOF
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig
metadata:
  name: ${CLUSTER_NAME}
  region: ${AWS_DEFAULT_REGION}
  version: "${K8S_VERSION}"
  tags:
    karpenter.sh/discovery: ${CLUSTER_NAME}
iam:
  withOIDC: true
  serviceAccounts:
  - metadata:
      name: karpenter
      namespace: ${KARPENTER_NAMESPACE}
    roleName: ${CLUSTER_NAME}-karpenter
    attachPolicyARNs:
    - arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:policy/KarpenterControllerPolicy-${CLUSTER_NAME}
iamIdentityMappings:
- arn: "arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/KarpenterNodeRole-${CLUSTER_NAME}"
  username: system:node:{{EC2PrivateDNSName}}
  groups:
  - system:bootstrappers
  - system:nodes
managedNodeGroups:
- name: ${CLUSTER_NAME}-ng
  desiredCapacity: 2
  minSize: 1
  maxSize: 10
EOF

TRUST_RELATIONSHIP_FILE="trust-relationship.json"
cat <<EOF >${TRUST_RELATIONSHIP_FILE}
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

# 1. Criar a função de IAM
aws iam create-role \
  --role-name ${IAM_ROLE_NAME} \
  --assume-role-policy-document file://${TRUST_RELATIONSHIP_FILE} \
  --description "Função IAM para acesso ao cluster EKS"
rm trust-relationship.json

# 1. Associar política ao usuário ou função de IAM
aws iam attach-role-policy --role-name $IAM_ROLE_NAME --policy-arn arn:aws:iam::aws:policy/AmazonEKSClusterPolicy

# 2. Atualizar o aws-auth ConfigMap para adicionar o usuário ou função de IAM
kubectl get configmap/aws-auth -n kube-system -o yaml >aws-auth.yaml
NEW_ENTRY="    - rolearn: ${USER_ARN}\n      username: admin\n      groups:\n        - system:masters"
sed -i "/mapRoles: |/a \    - rolearn: ${USER_ARN}\n      username: admin\n      groups:\n        - system:masters" aws-auth.yaml
kubectl apply -f aws-auth.yaml

# Instalando o CSI driver para o EBS
eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
eksctl create iamserviceaccount \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster ${CLUSTER_NAME} \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole
eksctl create addon --name aws-ebs-csi-driver --cluster ${CLUSTER_NAME} --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole --force

# Instalando o Karpenter
curl -fsSL https://raw.githubusercontent.com/aws/karpenter-provider-aws/main/website/content/en/v0.35/getting-started/getting-started-with-karpenter/cloudformation.yaml >$TEMPOUT &&
  aws cloudformation deploy \
    --stack-name "Karpenter-${CLUSTER_NAME}" \
    --template-file "${TEMPOUT}" \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameter-overrides "ClusterName=${CLUSTER_NAME}"

helm repo add karpenter https://charts.karpenter.sh/
helm repo update

export CLUSTER_ENDPOINT="$(aws eks describe-cluster --name ${CLUSTER_NAME} --query "cluster.endpoint" --output text)"
export KARPENTER_IAM_ROLE_ARN="arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/${CLUSTER_NAME}-karpenter"

echo $CLUSTER_ENDPOINT $KARPENTER_IAM_ROLE_ARN

aws iam create-service-linked-role --aws-service-name spot.amazonaws.com >/dev/null 2>&1 || true

kubectl apply -f https://raw.githubusercontent.com/aws/karpenter-provider-aws/main/pkg/apis/crds/karpenter.sh_nodepools.yaml
kubectl apply -f https://raw.githubusercontent.com/aws/karpenter-provider-aws/main/pkg/apis/crds/karpenter.k8s.aws_ec2nodeclasses.yaml
kubectl apply -f https://raw.githubusercontent.com/aws/karpenter-provider-aws/main/pkg/apis/crds/karpenter.sh_nodeclaims.yaml

helm upgrade --install karpenter karpenter/karpenter \
  --namespace karpenter --create-namespace \
  --set serviceAccount.create=true \
  --set-string serviceAccount.annotations."eks\.amazonaws\.com/role-arn"=${KARPENTER_IAM_ROLE_ARN} \
  --set clusterName=${CLUSTER_NAME} \
  --set clusterEndpoint=${CLUSTER_ENDPOINT} \
  --set aws.defaultInstanceProfile=KarpenterNodeInstanceProfile-${CLUSTER_NAME} \
  --wait

cat <<EOF | envsubst | kubectl apply -f -
apiVersion: karpenter.sh/v1beta1
kind: NodePool
metadata:
  name: default
spec:
  template:
    spec:
      requirements:
        - key: "kubernetes.io/arch"
          operator: In
          values: ["amd64"]
        - key: "kubernetes.io/os"
          operator: In
          values: ["linux"]
        - key: "karpenter.sh/capacity-type"
          operator: In
          values: ["on-demand"]
        - key: "topology.kubernetes.io/zone"
          operator: In
          values: ["${AWS_DEFAULT_REGION}a", "${AWS_DEFAULT_REGION}b"]
      nodeClassRef:
        name: default
  disruption:
    consolidationPolicy: WhenUnderutilized
    expireAfter: 720h

---
apiVersion: karpenter.k8s.aws/v1beta1
kind: EC2NodeClass
metadata:
  name: default
spec:
  amiFamily: AL2
  role: "KarpenterNodeRole-${CLUSTER_NAME}"
  subnetSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}"
  securityGroupSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}"
EOF
eksctl utils update-cluster-logging --enable-types=all --approve --region=${AWS_DEFAULT_REGION} --cluster=${CLUSTER_NAME}

# Instalando o ingress controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.10.0/deploy/static/provider/aws/deploy.yaml

# Instalando o prometheus
helm install prometheus prometheus-community/prometheus \
  --set alertmanager.disabled=true \
  --set kube-state-metrics.enabled=true \
  --set prometheus-node-exporter.enabled=false \
  --set prometheus-pushgateway.enabled=false \
  --namespace monitoring --create-namespace

# Instalando o load balancer
curl -O https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.5.4/docs/install/iam_policy.json
aws iam create-policy \
  --policy-name AWSLoadBalancerControllerIAMPolicy \
  --policy-document file://iam_policy.json >/dev/null 2>&1 || true

eksctl create iamserviceaccount \
  --cluster=${CLUSTER_NAME} \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --role-name AmazonEKSLoadBalancerControllerRole \
  --attach-policy-arn=arn:aws:iam::${AWS_ACCOUNT_ID}:policy/AWSLoadBalancerControllerIAMPolicy \
  --approve

helm repo add eks https://aws.github.io/eks-charts
helm repo update

helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
  -n kube-system \
  --set clusterName=${CLUSTER_NAME} \
  --set serviceAccount.create=false \
  --set serviceAccount.name=aws-load-balancer-controller

# Instalando o cert manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.5.3 --set installCRDs=true

kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    email: edu@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-account-key
    solvers:
    - http01:
        ingress:
          class: public
EOF

kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ${DOMAIN}-tls
spec:
  secretName: ${DOMAIN}-tls-secret
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: ${DOMAIN}
  dnsNames:
  - ${DOMAIN}
EOF

# deploy api-server
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/gitlab.yaml?ref_type=heads
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/jwt.yaml?ref_type=heads
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/api-service.yaml?ref_type=heads >api-server.yaml
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/user-service.yaml?ref_type=heads >user-service.yaml
envsubst <api-server.yaml | kubectl apply -f -
envsubst <user-service.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=api-server --timeout=300s
rm api-server.yaml
rm user-service.yaml

kubectl create ingress my-ingress --class=nginx \
  --annotation cert-manager.io/cluster-issuer=letsencrypt \
  --rule "${DOMAIN}/*=api-server-service:3000,tls=${DOMAIN}-tls-secret" \
  --rule "${DOMAIN}/users=user-service:3000,tls=${DOMAIN}-tls-secret"

eksctl update cluster \
  --name api-whatsapp \
  --version 1.29 \
  --region sa-east-1 \
  --nodegroup-name node-autoscaling \
  --node-type t3.large \
  --nodes-min 1 \
  --nodes-max 20 \
  --asg-access

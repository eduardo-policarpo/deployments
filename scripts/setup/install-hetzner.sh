#!/bin/bash

# Detectar sistema operacional
OS=$(uname -s)

# Função para instalar kubectl
install_kubectl() {
  echo "Instalando kubectl..."
  if [[ $OS == "Darwin" ]]; then
    brew install kubectl || { echo "Erro ao instalar kubectl"; exit 1; }
  elif [[ $OS == "Linux" ]]; then
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl || { echo "Erro ao instalar kubectl"; exit 1; }
  else
    echo "Sistema operacional não suportado para a instalação do kubectl."
    exit 1
  fi
}

# Função para instalar helm
install_helm() {
  echo "Instalando helm..."
  if [[ $OS == "Darwin" ]]; then
    brew install helm || { echo "Erro ao instalar helm"; exit 1; }
  elif [[ $OS == "Linux" ]]; then
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash || { echo "Erro ao instalar helm"; exit 1; }
  else
    echo "Sistema operacional não suportado para a instalação do helm."
    exit 1
  fi
}

# Função para instalar hetzner-k3s
install_hetzner_k3s() {
  echo "Instalando hetzner-k3s..."
  if [[ $OS == "Darwin" ]]; then
    brew install vitobotta/tap/hetzner_k3s || { echo "Erro ao instalar hetzner-k3s"; exit 1; }
  elif [[ $OS == "Linux" ]]; then
    wget https://github.com/vitobotta/hetzner-k3s/releases/download/v1.1.5/hetzner-k3s-linux-amd64
    chmod +x hetzner-k3s-linux-amd64
    sudo mv hetzner-k3s-linux-amd64 /usr/local/bin/hetzner-k3s || { echo "Erro ao instalar hetzner-k3s"; exit 1; }
  else
    echo "Sistema operacional não suportado para a instalação do hetzner-k3s."
    exit 1
  fi
}

# Instalar ferramentas necessárias
install_kubectl
install_helm
install_hetzner_k3s

# Solicitar API Key
read -p "Please enter your API key: " HCLOUD_TOKEN

# Solicitar o Domain
read -p "Please enter your domain: " DOMAIN

# criar o cluster
hetzner-k3s create --config - <<EOF
hetzner_token: $HCLOUD_TOKEN
cluster_name: ap1-cluster
kubeconfig_path: "./kubeconfig"
k3s_version: v1.26.4+k3s1
public_ssh_key_path: "~/.ssh/id_rsa.pub"
private_ssh_key_path: "~/.ssh/id_rsa"
use_ssh_agent: false 
ssh_allowed_networks:
  - 0.0.0.0/0 
api_allowed_networks:
  - 0.0.0.0/0 
private_network_subnet: 10.0.0.0/16 
disable_flannel: false 
schedule_workloads_on_masters: false
cloud_controller_manager_manifest_url: "https://github.com/hetznercloud/hcloud-cloud-controller-manager/releases/download/v1.18.0/ccm-networks.yaml"
csi_driver_manifest_url: "https://raw.githubusercontent.com/hetznercloud/csi-driver/v2.5.1/deploy/kubernetes/hcloud-csi.yml"
system_upgrade_controller_manifest_url: "https://raw.githubusercontent.com/rancher/system-upgrade-controller/master/manifests/system-upgrade-controller.yaml"
masters_pool:
  instance_type: cpx21
  instance_count: 1
  location: nbg1
worker_node_pools:
- name: small-static
  instance_type: cpx21
  instance_count: 3
  location: hel1
- name: big-autoscaled
  instance_type: cpx31
  instance_count: 2
  location: fsn1
  autoscaling:
    enabled: true
    min_instances: 0
    max_instances: 3
EOF

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

cat <<EOF > values.yaml
controller:
  replicaCount: 1
  service:
    enabled: true
    type: LoadBalancer
    annotations:
      load-balancer.hetzner.cloud/location: hel1
      load-balancer.hetzner.cloud/use-private-ip: "true"
      load-balancer.hetzner.cloud/hostname: $DOMAIN
      load-balancer.hetzner.cloud/http-redirect-https: 'false'
      load-balancer.hetzner.cloud/name: lb-ingress-nginx
      load-balancer.hetzner.cloud/uses-proxyprotocol: 'true'
  resources:
    limits:
      cpu: "250m"
      memory: "250Mi"
    requests:
      cpu: "100m"
      memory: "100Mi"
  ingressClassResource:
    name: nginx
    enabled: true
    default: true
    controllerValue: "k8s.io/ingress-nginx"
  ingressClassByName: true
EOF

# deploy ingress-nginx
helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx -f values.yaml --namespace ingress-nginx --create-namespace
rm values.yaml


helm repo add jetstack https://charts.jetstack.io
helm repo update

cat <<EOF > values.yaml
installCRDs: true
ingressShim:
  defaultIssuerName: letsencrypt-prod
  defaultIssuerKind: ClusterIssuer
prometheus:
  enabled: false
  servicemonitor:
    enabled: false
resources:
  requests:
    cpu: 10m
    memory: 32Mi
  limits:
    cpu: 100m
    memory: 100Mi
EOF

helm upgrade --install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace -f values.yaml


kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
  namespace: cert-manager
spec:
  acme:
    email: edu.poli@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class: nginx
EOF

kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ${DOMAIN}-tls
  namespace: default
spec:
  secretName: ${DOMAIN}-tls-secret
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
  commonName: ${DOMAIN}
  dnsNames:
  - ${DOMAIN}
EOF

kubectl create ingress my-ingress --class=nginx \
    --annotation nginx.ingress.kubernetes.io/rewrite-target="/" \
    --annotation cert-manager.io/cluster-issuer="letsencrypt-prod" \
    --rule "${DOMAIN}/*=api-server-service:3000,tls=${DOMAIN}-tls-secret" \
    --dry-run=client -o yaml > ingress.yaml



# deploy rabbitmq
cat <<EOF > values.yaml
service:
  type: LoadBalancer
  annotations:
    load-balancer.hetzner.cloud/location: "hel1"
    load-balancer.hetzner.cloud/use-private-ip: "true"
    load-balancer.hetzner.cloud/name: lb-rabbitmq
EOF

helm install rabbitmq-cluster bitnami/rabbitmq \
  --set image.repository=edupoli/rabbitmq \
  --set image.tag=1.0.6 \
  --set auth.username=admin \
  --set auth.password=admin \
  --set persistence.enabled=true \
  --set persistence.size=25Gi \
  --namespace infra \
  -f values.yaml

rm values.yaml

# install kube-prometheus-stack
helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
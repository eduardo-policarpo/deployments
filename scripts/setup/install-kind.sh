#!/bin/bash

# Definição de cores para saída no terminal
readonly RED="\033[1;31m"
readonly WHITE="\033[1;37m"
readonly GREEN="\033[1;32m"
readonly GRAY_LIGHT="\033[0;37m"
readonly YELLOW="\033[1;33m"
readonly CYAN="\033[1;36m"
readonly NC="\033[0m" # Sem cor

# Definição de variáveis globais
export DOMAIN
export HTTP_PROTOCOL
export APP_URL
export API_URL
export USERS_URL
export MONGODB_ROOT_PASSWORD


# Verifica se o script está sendo executado como root
if [ "$EUID" -ne 0 ]; then
  printf "${YELLOW}⚠️ Este script deve ser executado como root. Use 'sudo ./setup.sh${NC}\n"
  exit 1
fi

mkdir -p /${HOME}/API/api-data
chmod -R777 /${HOME}/API

# Função para obter o IP público
get_public_ip() {
  local ip
  local services=(
    "https://api.ipify.org"
    "https://ipinfo.io/ip"
    "http://ifconfig.me"
    "https://ipecho.net/plain"
    "https://ifconfig.co"
    "https://myexternalip.com/raw"
  )

  for service in "${services[@]}"; do
    ip=$(timeout 5 curl -s "$service")
    if [[ "$ip" =~ ^[0-9]{1,3}(\.[0-9]{1,3}){3}$ ]]; then
      echo "$ip"
      return 0
    fi
  done

  printf "${RED}❌  Não foi possível determinar o IP público.${GRAY_LIGHT}\n"
  exit 1
}

# Função para validar se o domínio aponta para o IP público correto
validate_domain() {
  local host=$1
  local vps_ip=$(get_public_ip)
  local ip=$(dig +short A "$host")

  if [ -z "$ip" ]; then
    return 1
  elif [ "$ip" == "$vps_ip" ]; then
    return 0
  else
    return 2
  fi
}

# Função para adicionar o domínio e subdomínios ao arquivo /etc/hosts
add_to_hosts() {
  local ip=$(hostname -I | awk '{print $1}')
  local domain=$1
  local subdomains=("api" "users")
  local domains=("$domain")

  # Adicionar subdomínios à lista de domínios
  for sub in "${subdomains[@]}"; do
    domains+=("$sub.$domain")
  done

  # Adicionar domínios ao /etc/hosts
  for d in "${domains[@]}"; do
    local host_entry="$ip $d"
    if ! grep -q "$host_entry" /etc/hosts; then
      echo "$host_entry" | sudo tee -a /etc/hosts >/dev/null
      printf "${GREEN}✔️  Domínio $d adicionado ao /etc/hosts com IP $ip.${NC}\n"
    else
      printf "${YELLOW}⚠️  Domínio $d já existe no /etc/hosts.${NC}\n"
    fi
  done
}

# Função para instalar Docker
install_docker() {
  if ! command -v docker &>/dev/null; then
    printf "${CYAN}⚠️  Docker não encontrado. Instalando Docker....${NC}\n"
    # Baixar e executar o script de instalação do Docker
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    printf "${GREEN}✔️  Docker foi instalado com sucesso!.${NC}\n"
  else
    printf "${YELLOW}⚠️  Docker ja esta instalado!.${NC}\n"

  fi
}

# Função para instalar Kind
install_kind() {
  if ! command -v kind &>/dev/null; then
    printf "${CYAN}⚠️  Kind não encontrado. Instalando Kind...${NC}\n"
    # Verifica a arquitetura e baixa a versão correspondente do Kind
    if [ $(uname -m) = x86_64 ]; then
      curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.23.0/kind-linux-amd64
    elif [ $(uname -m) = aarch64 ]; then
      curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.23.0/kind-linux-arm64
    fi
    chmod +x ./kind
    sudo mv ./kind /usr/local/bin/kind
    printf "${GREEN}✔️ Kind foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Kind já está instalado!.${NC}\n"
  fi
}

# Função para instalar Kubectl
install_kubectl() {
  if ! command -v kubectl &>/dev/null; then
    printf "${CYAN}⚠️  Kubectl não encontrado. Instalando Kubectl via Snap...${NC}\n"
    sudo snap install kubectl --classic
    printf "${GREEN}✔️ Kubectl foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Kubectl já está instalado!.${NC}\n"
  fi
}

# Função para instalar Helm
install_helm() {
  if ! command -v helm &>/dev/null; then
    printf "${CYAN}⚠️  helm não encontrado. Instalando helm via Snap...${NC}\n"
    sudo snap install helm --classic
    printf "${GREEN}✔️ helm foi instalado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  helm já está instalado!.${NC}\n"
  fi
}

# Função para configurar vm.max_map_count
configure_vm_max_map_count() {
  printf "${CYAN}⚠️  Configurando vm.max_map_count para 1677720...${NC}\n"
  # Verificar se a linha já existe e substituí-la, se necessário, ou adicioná-la
  if grep -q "vm.max_map_count" /etc/sysctl.conf; then
    sudo sed -i 's/^vm.max_map_count.*/vm.max_map_count=1677720/' /etc/sysctl.conf
  else
    echo 'vm.max_map_count=1677720' | sudo tee -a /etc/sysctl.conf >/dev/null
  fi
  # Aplicar a mudança
  sudo sysctl -p
  printf "${GREEN}✔️ vm.max_map_count configurado com sucesso!${NC}\n"
}

# Função para criar o cluster
create_kind_cluster() {
  if ! kind get clusters | grep -q api-server; then
    printf "${CYAN}⚠️ Criando o cluster api-server...${NC}\n"
    kind create cluster --name api-server --config - <<EOF
apiVersion: kind.x-k8s.io/v1alpha4
kind: Cluster
networking:
  apiServerAddress: $(hostname -I | awk '{print $1}')
  apiServerPort: 6443
nodes:
  - role: control-plane
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 80
        listenAddress: 0.0.0.0
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        listenAddress: 0.0.0.0
        protocol: TCP
  - role: worker
    extraMounts:
    - hostPath: /$HOME/API
      containerPath: /mnt
      propagation: None
      readOnly: false
EOF

    # habilitando metrics no kind
    kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml
    kubectl patch deployment metrics-server -n kube-system --patch "$(
      cat <<EOF
spec:
  template:
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls
        name: metrics-server
EOF
    )"
    printf "${GREEN}✔️ Cluster api-server criado com sucesso!${NC}\n"
  else
    printf "${YELLOW}⚠️  Cluster api-server já existe!.${NC}\n"
  fi
}

# Aguarde até que o cluster esteja pronto
wait_for_kind_cluster_ready() {
  printf "${CYAN}⚠️  Aguardando a implantação do cluster...${NC}\n"
  # Loop até que todos os nós estejam no estado "Ready"
  until kubectl get nodes | grep -q 'Ready'; do
    sleep 1
  done
  printf "${GREEN}✔️ Cluster status Ready!${NC}\n"
}


while true; do
  printf "${WHITE}💻 Insira o domínio para a API:${GRAY_LIGHT} "
  read -r DOMAIN

  if [ -z "$DOMAIN" ]; then
    printf "${YELLOW}❕ O domínio é obrigatório. Você deve informar um valor.❕ ${GRAY_LIGHT}\n"
    continue
  fi

  printf "${WHITE}Você deseja gerar um certificado autoassinado para desenvolvimento ou um certificado para produção?${GRAY_LIGHT}\n"
  printf "Digite 'd' para desenvolvimento ou 'p' para produção: "
  read -r cert_type

  if [[ "$cert_type" == "d" ]]; then
    HTTP_PROTOCOL="http"
    add_to_hosts "$DOMAIN"
    break
  elif [[ "$cert_type" == "p" ]]; then
    HTTP_PROTOCOL="https"
    if validate_domain "$DOMAIN"; then
      printf "${GREEN}$DOMAIN está correto e aponta para o IP da VPS.❓${GRAY_LIGHT}\n"
      read -r -p "Digite 's' para sim, 'n' para não: " response
      if [[ "$response" == "s" ]]; then
        break
      fi
    else
      printf "${YELLOW}❕ O domínio $DOMAIN não aponta para o IP da VPS ou não existe.${GRAY_LIGHT}\n"
      read -r -p "Digite 's' para informar um novo domínio, 'n' para cancelar a instalação: " response
      if [[ "$response" == "n" ]]; then
        printf "${RED}❌ Instalação cancelada pelo usuário.${GRAY_LIGHT}\n"
        exit 1
      fi
    fi
  else
    printf "${YELLOW}❕ Opção inválida. Por favor, escolha 'd' para desenvolvimento ou 'p' para produção.${GRAY_LIGHT}\n"
  fi
done

# Atribuicao de valores as variaveis
APP_URL="${HTTP_PROTOCOL}://${DOMAIN}"
API_URL="${HTTP_PROTOCOL}://api.${DOMAIN}"
USERS_URL="${HTTP_PROTOCOL}://users.${DOMAIN}"

# Solicitar senha do MongoDB com validação e confirmação
while true; do
  printf "${WHITE}Digite a senha para o root do MongoDB (não pode ser vazia):${GRAY_LIGHT} "
  read -r -s mongodb_password
  printf "\n${WHITE}Confirme a senha:${GRAY_LIGHT} "
  read -r -s mongodb_password_confirm
  printf "\n"

  if [[ -z "$mongodb_password" || -z "$mongodb_password_confirm" ]]; then
    printf "${RED}Erro: A senha não pode ser vazia.${NC}\n"
  elif [[ "$mongodb_password" != "$mongodb_password_confirm" ]]; then
    printf "${RED}Erro: As senhas não coincidem.${NC}\n"
  else
    printf "${GREEN}Senha do MongoDB definida.${NC}\n"
    break
  fi
done

MONGODB_ROOT_PASSWORD=$mongodb_password

sudo apt-get update
sudo apt install -y snapd
configure_vm_max_map_count
install_docker
install_kind
install_kubectl
install_helm
create_kind_cluster
wait_for_kind_cluster_ready 
helm repo add jetstack https://charts.jetstack.io
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# Instala o Prometheus
helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace

# Instala o cert-manager
helm install cert-manager jetstack/cert-manager --version v1.5.0 --set installCRDs=true --namespace cert-manager --create-namespace
kubectl wait --namespace cert-manager --for=condition=ready pod --selector=app.kubernetes.io/name=cert-manager --timeout=180s

# Criar ClusterIssuer para produção
if [[ "$cert_type" == "p" ]]; then
  kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    email: seu-email@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-account-key
    solvers:
    - http01:
        ingress:
          class: public
EOF
  kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ${DOMAIN}-tls
  namespace: default
spec:
  secretName: ${DOMAIN}-tls-secret
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: ${DOMAIN}
  dnsNames:
  - ${DOMAIN}
EOF
fi

# deploy rabbitmq
helm install rabbitmq-cluster bitnami/rabbitmq \
  --set communityPlugins="https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.12.0/rabbitmq_delayed_message_exchange-3.12.0.ez" \
  --set extraPlugins="rabbitmq_delayed_message_exchange" \
  --set replicaCount=1 \
  --set auth.username=admin \
  --set auth.password=admin \
  --set auth.securePassword=false \
  --set persistence.enabled=true \
  --set persistence.size=5Gi \
  --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
  --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
  --set service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
  --set service.type=NodePort \
  --set service.nodePorts.amqp=30072 \
  --set service.nodePorts.management=30073 \
  --namespace infra --create-namespace

# deploy redis
helm install redis-server bitnami/redis \
  --set master.persistence.size=2Gi \
  --set architecture="replication" \
  --set replicaCount=1 \
  --set master.service.type=NodePort \
  --set master.service.nodePorts.redis=30007 \
  --set master.service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
  --set auth.enabled=false \
  --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
  --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
  --namespace infra --create-namespace

# deploy mongodb
helm install mongodb-standalone bitnami/mongodb \
  --set image.repository=bitnami/mongodb \
  --set image.tag=7.0.11 \
  --set image.pullPolicy=Always \
  --set architecture="standalone" \
  --set auth.enabled=true \
  --set auth.rootPassword=$MONGODB_ROOT_PASSWORD \
  --set auth.usernames={admin} \
  --set auth.passwords={admin} \
  --set auth.databases={wappi} \
  --set externalAccess.autoDiscovery.resourcesPreset="medium" \
  --set service.type=NodePort \
  --set service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
  --set service.nodePorts.mongodb=30676 \
  --set externalAccess.service.nodePorts[0]=30676 \
  --set externalAccess.service.type=NodePort \
  --namespace infra --create-namespace

# Aguardar os pods do MongoDB estarem prontos
printf "${YELLOW}⚠️  Aguardando MongoDB ficar com Status Ready...${NC}\n"
kubectl rollout status deployment/mongodb-standalone -n infra --timeout=600s

# Aplicar o job para configurar o MongoDB
printf "${GREEN}✔️ Aplicando o job de configuração do MongoDB...${NC}\n"
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/mongodb/mongodb-setup-job.yaml?ref_type=heads
printf "${YELLOW}⚠️  Verificando o status do job de configuração do MongoDB...${NC}\n"
kubectl wait --for=condition=complete job/mongodb-setup -n infra --timeout=300s

# aplicar secrets
printf "${GREEN}✔️ Aplicando as secrets no cluster...${NC}\n"
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/gitlab-registry.yaml?ref_type=heads
kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/jwt-secret.yaml?ref_type=heads

# deploy client-service
printf "${GREEN}✔️ Deploy e configuracao da aplicacao client-service...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/kubernetes/client-service.yaml?ref_type=heads >client-service.yaml
envsubst <client-service.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=client-service --timeout=300s
rm client-service.yaml

# deploy api-server
printf "${GREEN}✔️ Deploy e configuracao da aplicacao api-server...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/kubernetes/api-service.yaml?ref_type=heads >api-service.yaml
export APP_URL="${API_URL}"
envsubst <api-service.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=api-service --timeout=300s
rm api-service.yaml

# deploy user-service
printf "${GREEN}✔️ Deploy e configuracao da aplicacao user-service...${NC}\n"
curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/kubernetes/user-service.yaml?ref_type=heads >user-service.yaml
export APP_URL="${USERS_URL}"
envsubst <user-service.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=user-service --timeout=300s
rm user-service.yaml

# deploy ingress controller
printf "${GREEN}✔️ Deploy e configuracao do ingress controller...${NC}\n"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=180s

# Esperar pelo webhook de admissão ficar pronto
while ! kubectl get --raw "/apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations/ingress-nginx-admission" &>/dev/null; do
  printf "${YELLOW}⚠️  Aguardando o webhook de admissão do Ingress ficar com Status Ready...${NC}\n"
  sleep 10
done

# Criar Ingress
cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
$(if [[ "$cert_type" == "p" ]]; then
  echo "  annotations:"
  echo "    cert-manager.io/cluster-issuer: letsencrypt"
fi)
spec:
  ingressClassName: nginx
$(if [[ "$cert_type" == "p" ]]; then
  echo "  tls:"
  echo "  - hosts:"
  echo "    - ${DOMAIN}"
  echo "    - api.${DOMAIN}"
  echo "    - users.${DOMAIN}"
  echo "    secretName: ${DOMAIN}-tls-secret"
fi)
  rules:
  - host: ${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: client-service-svc
            port:
              number: 3000
  - host: api.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: api-service-svc
            port:
              number: 3000
  - host: users.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: user-service-svc
            port:
              number: 3000
EOF

kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: api-data
provisioner: kubernetes.io/no-provisioner
reclaimPolicy: Retain
volumeBindingMode: WaitForFirstConsumer

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: api-storage
spec:
  storageClassName: api-data
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: api-data-pv
spec:
  storageClassName: api-data
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/api-data"
EOF

# export KUBECONFIG
kubectl config view --raw >kubeConfig.yaml

printf "${GREEN} INSTALACAO CONCLUIDA 🚀🚀🚀🚀${GRAY_LIGHT} "

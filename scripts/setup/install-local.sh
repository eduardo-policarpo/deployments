#!/bin/bash

export DOMAIN=app.domain.local

sudo apt-get update
sudo apt install -y ipcalc
sudo apt install -y snapd
sudo snap install microk8s --classic
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube

# criando alias para kubectl
add_alias() {
  local alias_line="$1"
  local alias_name=$(echo "$alias_line" | cut -d'=' -f1)
  local bashrc="${HOME}/.bashrc"

  if ! grep -qF "$alias_name" "$bashrc"; then
    echo "$alias_line" >>"$bashrc"
    echo "Alias $alias_name adicionado ao $bashrc"
  else
    echo "Alias $alias_name já existe no $bashrc"
  fi
}

add_alias "alias kubectl='microk8s kubectl'"
add_alias "alias k='microk8s kubectl'"

microk8s enable metrics-server
microk8s enable observability
microk8s kubectl create namespace infra

microk8s enable ingress

microk8s enable helm

microk8s helm repo add bitnami https://charts.bitnami.com/bitnami

microk8s helm repo update

microk8s helm install rabbitmq-cluster bitnami/rabbitmq \
  --set communityPlugins=https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.12.0/rabbitmq_delayed_message_exchange-3.12.0.ez \
  --set extraPlugins="rabbitmq_delayed_message_exchange" \
  --set auth.username=admin \
  --set auth.password=admin \
  --set persistence.enabled=true \
  --set persistence.size=25Gi \
  --set service.type=ClusterIP \
  --namespace infra

microk8s helm install mongodb-cluster bitnami/mongodb \
  --set architecture=replicaset \
  --set auth.rootPassword=admin123 \
  --set auth.username=admin \
  --set auth.password=admin \
  --set auth.database=wappi \
  --namespace infra

# deploy api-server
microk8s kubectl apply -f https://raw.githubusercontent.com/edupoli/whatsapp-api/master/gitlab.yaml
curl -s https://raw.githubusercontent.com/edupoli/whatsapp-api/master/api-service.yaml >api-server.yaml
envsubst <api-server.yaml | microk8s kubectl apply -f -
microk8s kubectl wait --for=condition=ready pod -l app=api-server --timeout=300s
rm api-server.yaml

microk8s kubectl create ingress my-ingress --class=nginx \
  --rule "${DOMAIN}/*=api-server-service:3000"

microk8s config >kubeconfig.yaml
sed -i "s|server: https://.*:16443|server: https://${DOMAIN}:16443|" kubeconfig.yaml
sed -i '/certificate-authority-data/d' kubeconfig.yaml
sed -i '/server: https:\/\//a \ \ \ \ insecure-skip-tls-verify: true' kubeconfig.yaml
sed -i 's|microk8s-cluster|api-server|g' kubeconfig.yaml

microk8s kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: api-data
provisioner: microk8s.io/hostpath
reclaimPolicy: Retain
parameters:
  pvDir: /home/API
volumeBindingMode: WaitForFirstConsumer

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: api-storage
spec:
  storageClassName: api-data
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 60Gi
EOF

printf "${GREEN} INSTALACAO CONCLUIDA 🚀🚀🚀🚀${GRAY_LIGHT} "

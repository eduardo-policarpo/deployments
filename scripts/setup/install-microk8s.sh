#!/bin/bash

readonly RED="\033[1;31m"
readonly WHITE="\033[1;37m"
readonly GREEN="\033[1;32m"
readonly GRAY_LIGHT="\033[0;37m"
readonly YELLOW="\033[1;33m"
readonly NC="\033[0m" # No Color

get_public_ip() {
  local ip
  local services=(
    "https://api.ipify.org"
    "https://ipinfo.io/ip"
    "http://ifconfig.me"
    "https://ipecho.net/plain"
    "https://ifconfig.co"
    "https://myexternalip.com/raw"
  )

  for service in "${services[@]}"; do
    ip=$(timeout 5 curl -s "$service")
    if [[ "$ip" =~ ^[0-9]{1,3}(\.[0-9]{1,3}){3}$ ]]; then
      echo "$ip"
      return 0
    fi
  done

  printf "${RED}❌  Não foi possível determinar o IP público.${GRAY_LIGHT}\n"
  exit 1
}

validate_domain() {
  local host=$1
  local ip
  local vps_ip=$(get_public_ip)

  if [ $? -ne 0 ]; then
    printf "${RED}❌  Erro ao obter o IP público da VPS.${GRAY_LIGHT}\n"
    exit 1
  fi

  ip=$(dig A "$host" +noall +answer | awk '{print $NF}')
  if [ -z "$ip" ]; then
    if printf "${YELLOW}❕ O domínio informado não existe. Deseja informar um novo domínio?${GRAY_LIGHT}\n\n" && read -r -p "Digite 's' para sim, 'n' para não: " response && [[ "$response" == "s" ]]; then
      return 1
    else
      printf "${RED}❌ Instalação cancelada pelo usuário.${GRAY_LIGHT}\n"
      exit 1
    fi
  elif [ "$ip" == "$vps_ip" ]; then
    return 0
  else
    if printf "${YELLOW}❕ O domínio informado não aponta para o IP desta VPS. Deseja informar um novo domínio?${GRAY_LIGHT}\n\n" && read -r -p "Digite 's' para sim, 'n' para não: " response && [[ "$response" == "s" ]]; then
      return 1
    else
      printf "${RED}❌ Instalação cancelada pelo usuário.${GRAY_LIGHT}\n"
      exit 1
    fi
  fi
}

while true; do
  export DOMAIN
  printf "${WHITE}💻 Insira o domínio para a API:${GRAY_LIGHT} "
  read -r DOMAIN response

  if [ -z "$DOMAIN" ]; then
    printf "${YELLOW}❕ O domínio é obrigatório. Você deve informar um valor.❕ ${GRAY_LIGHT}\n"
    continue
  fi

  if validate_domain "$DOMAIN"; then
    printf "${GREEN}$DOMAIN está correto ❓${GRAY_LIGHT}\n"
    read -r -p "Digite 's' para sim, 'n' para não: " response
    if [[ "$response" == "s" ]]; then
      break
    fi
  fi
done

# Solicitar senha do RabbitMQ com validação
while true; do
  export RABBITMQ_PASSWORD
  printf "${WHITE}Digite a senha para o admin do RabbitMQ (não pode ser vazia):${GRAY_LIGHT} "
  read -r -s RABBITMQ_PASSWORD
  printf "\n${WHITE}Confirme a senha:${GRAY_LIGHT} "
  read -r -s RABBITMQ_PASSWORD_CONFIRM
  if [[ -z "$RABBITMQ_PASSWORD" ]]; then
    printf "\n${RED}Erro: A senha não pode ser vazia.${NC}\n"
  elif [[ "$RABBITMQ_PASSWORD" != "$RABBITMQ_PASSWORD_CONFIRM" ]]; then
    printf "\n${RED}Erro: As senhas não coincidem.${NC}\n"
  else
    printf "\n${GREEN}Senha do RabbitMQ definida.${NC}\n"
    break
  fi
done

# Solicitar senha do MongoDB com validação
while true; do
  export MONGODB_PASSWORD
  printf "${WHITE}Digite a senha para o root do MongoDB (não pode ser vazia):${GRAY_LIGHT} "
  read -r -s MONGODB_PASSWORD
  printf "\n${WHITE}Confirme a senha:${GRAY_LIGHT} "
  read -r -s MONGODB_PASSWORD_CONFIRM
  if [[ -z "$MONGODB_PASSWORD" ]]; then
    printf "\n${RED}Erro: A senha não pode ser vazia.${NC}\n"
  elif [[ "$MONGODB_PASSWORD" != "$MONGODB_PASSWORD_CONFIRM" ]]; then
    printf "\n${RED}Erro: As senhas não coincidem.${NC}\n"
  else
    printf "\n${GREEN}Senha do MongoDB definida.${NC}\n"
    break
  fi
done

sudo apt-get update
sudo apt install -y snapd
sudo apt-get install sshpass
sudo snap install microk8s --classic
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube

# criando alias para kubectl
add_alias() {
  local alias_line="$1"
  local alias_name=$(echo "$alias_line" | cut -d'=' -f1)
  local bashrc="${HOME}/.bashrc"

  if ! grep -qF "$alias_name" "$bashrc"; then
    echo "$alias_line" >>"$bashrc"
    echo "Alias $alias_name adicionado ao $bashrc"
  else
    echo "Alias $alias_name já existe no $bashrc"
  fi
}

add_alias "alias kubectl='microk8s kubectl'"
add_alias "alias k='microk8s kubectl'"

microk8s enable dns
microk8s enable hostpath-storage
microk8s enable metrics-server
microk8s enable observability
microk8s kubectl create namespace infra
microk8s enable cert-manager

microk8s kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    email: edu@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-account-key
    solvers:
    - http01:
        ingress:
          class: public
EOF

microk8s kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ${DOMAIN}-tls
spec:
  secretName: ${DOMAIN}-tls-secret
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: ${DOMAIN}
  dnsNames:
  - ${DOMAIN}
EOF

microk8s enable ingress
microk8s enable helm
microk8s helm repo add bitnami https://charts.bitnami.com/bitnami
microk8s helm repo update

microk8s helm install rabbitmq-cluster bitnami/rabbitmq \
  --set communityPlugins="https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.12.0/rabbitmq_delayed_message_exchange-3.12.0.ez" \
  --set extraPlugins="rabbitmq_delayed_message_exchange" \
  --set auth.username=admin \
  --set auth.password="$RABBITMQ_PASSWORD" \
  --set persistence.enabled=true \
  --set persistence.size=8Gi \
  --set service.type=NodePort \
  --set service.nodePorts.amqp=30672 \
  --set service.nodePorts.manager=31672 \
  --namespace infra --create-namespace

microk8s helm install mongodb-cluster bitnami/mongodb \
  --set image.repository=bitnami/mongodb \
  --set image.tag=7.0.11 \
  --set image.pullPolicy=Always \
  --set architecture="replicaset" \
  --set replicaCount=2 \
  --set auth.enabled=true \
  --set auth.rootPassword="$MONGODB_PASSWORD" \
  --set auth.usernames={admin} \
  --set auth.passwords={admin} \
  --set auth.databases={wappi} \
  --set externalAccess.enabled=true \
  --set externalAccess.service.type=NodePort \
  --set externalAccess.externalMaster.port=27017 \
  --set externalAccess.service.nodePorts[0]=32027 \
  --set externalAccess.service.nodePorts[1]=32018 \
  --set resources.requests.memory="500Mi" \
  --set resources.requests.cpu="500m" \
  --set resources.limits.memory="2Gi" \
  --set resources.limits.cpu="1" \
  --set arbiter.resources.requests.memory="256Mi" \
  --set arbiter.resources.requests.cpu="250m" \
  --set arbiter.resources.limits.memory="512Mi" \
  --set arbiter.resources.limits.cpu="500m" \
  --set persistence.enabled=true \
  --set persistence.size=8Gi \
  --set persistence.storageClass="microk8s-hostpath" \
  --set persistence.accessModes={ReadWriteOnce} \
  --namespace infra --create-namespace

# Aguardar os pods do MongoDB estarem prontos
echo "Aguardando MongoDB ficar pronto..."
microk8s kubectl rollout status statefulset/mongodb-cluster -n infra --timeout=600s

# Verificar se o MongoDB está aceitando conexões
while true; do
  response=$(microk8s kubectl run --namespace infra mongodb-cluster-client --rm -i \
    --restart='Never' \
    --image docker.io/bitnami/mongodb:7.0.11 \
    --command -- mongosh \
    --host mongodb-cluster-0.mongodb-cluster-headless.infra.svc.cluster.local:27017,mongodb-cluster-1.mongodb-cluster-headless.infra.svc.cluster.local:27017 \
    --authenticationDatabase admin \
    -u root \
    -p $MONGODB_PASSWORD \
    --quiet \
    --eval "db.adminCommand('ping')" | grep 'ok: 1')

  if [[ $response =~ 'ok: 1,' ]]; then
    echo "MongoDB está aceitando conexões."
    break
  else
    echo "Aguardando o MongoDB aceitar conexões..."
    sleep 10
  fi
done

# Aplicar o job para configurar o MongoDB
echo "Aplicando o job de configuração do MongoDB..."
microk8s kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/mongodb/mongodb-setup-job.yaml?ref_type=heads

# Verificar o status do job
echo "Verificando o status do job de configuração do MongoDB..."
microk8s kubectl wait --for=condition=complete job/mongodb-setup -n infra --timeout=300s

# criar secrets com as credenciais do serviço SMTP
#microk8s kubectl create secret generic smtp-credentials --from-literal=smtp-user=AKIA6QM265B5BXS6UQNY --from-literal=smtp-pass=BJO4bwEzRuKOUWnsu47jVyVoPd+IJCBQustqfJvb9c1e --namespace default

# deploy api-server
microk8s kubectl apply -f https://gitlab.com/worker-group/deployments/-/raw/main/manifests/secrets/gitlab-registry.yaml?ref_type=heads
microk8s kubectl apply -f https://gitlab.com/worker-group/deployments/-/raw/main/manifests/secrets/jwt-secret.yaml?ref_type=heads
curl -s https://gitlab.com/worker-group/deployments/-/raw/main/manifests/kubernetes/api-service.yaml?ref_type=heads >api-server.yaml
curl -s https://gitlab.com/worker-group/deployments/-/raw/main/manifests/kubernetes/user-service.yaml?ref_type=heads >user-service.yaml
envsubst <api-server.yaml | microk8s kubectl apply -f -
envsubst <user-service.yaml | microk8s kubectl apply -f -
microk8s kubectl wait --for=condition=ready pod -l app=api-server --timeout=300s
rm api-server.yaml
rm user-service.yaml

microk8s kubectl create ingress my-ingress --class=nginx \
  --annotation cert-manager.io/cluster-issuer=letsencrypt \
  --rule "${DOMAIN}/*=api-server-service-service:3000,tls=${DOMAIN}-tls-secret"

microk8s config >kubeconfig.yaml
sed -i "s|server: https://.*:16443|server: https://${DOMAIN}:16443|" kubeconfig.yaml
sed -i '/certificate-authority-data/d' kubeconfig.yaml
sed -i '/server: https:\/\//a \ \ \ \ insecure-skip-tls-verify: true' kubeconfig.yaml
sed -i 's|microk8s-cluster|api-server|g' kubeconfig.yaml

microk8s kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: api-data
provisioner: microk8s.io/hostpath
reclaimPolicy: Retain
parameters:
  pvDir: /home/API
volumeBindingMode: WaitForFirstConsumer

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: api-storage
spec:
  storageClassName: api-data
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 60Gi
EOF

printf "${GREEN} INSTALACAO CONCLUIDA 🚀🚀🚀🚀${GRAY_LIGHT} "

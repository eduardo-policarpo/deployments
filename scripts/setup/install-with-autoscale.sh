#!/bin/bash

# Função para verificar se uma ferramenta está instalada
check_tool_installed() {
    if ! command -v $1 &> /dev/null
    then
        echo "$1 não está instalado."
        return 1
    else
        echo "$1 já está instalado."
        return 0
    fi
}


# Função para verificar e configurar o AWS CLI
check_configure_aws_cli() {
    check_tool_installed aws
    if [ $? -eq 1 ]; then
        echo "Instalando o AWS CLI..."
        if [[ "$OSTYPE" == "darwin"* ]]; then
            # macOS
            curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
            sudo installer -pkg AWSCLIV2.pkg -target /
        elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
            # Linux
            curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            unzip awscliv2.zip
            sudo ./aws/install
        else
            echo "Sistema operacional não suportado."
            exit 1
        fi

  echo "AWS CLI instalado com sucesso."
  echo "Por favor, crie sua ACCESS KEY e SECRET KEY acessando o serviço IAM em: https://console.aws.amazon.com/iam/"
  echo "Após ter os dados, execute o comando 'aws configure' no terminal e preencha os dados."
  echo "Rode este script novamente após configurar as credenciais."
  exit 0
    else
        echo "Verificando se as credenciais da AWS estão configuradas..."
        if ! aws sts get-caller-identity &> /dev/null; then
            echo "Credenciais da AWS não configuradas ou inválidas."
            echo "Por favor, configure suas credenciais da AWS executando 'aws configure'."
            exit 1
        else
            echo "Credenciais da AWS verificadas com sucesso."
        fi
    fi
}

# Verifica e configura o AWS CLI
check_configure_aws_cli



# Função para verificar se uma ferramenta está instalada
check_tool() {
  if ! command -v $1 &> /dev/null; then
    echo "$1 não está instalado."
    return 1
  else
    echo "$1 já está instalado."
    return 0
  fi
}

# Verificar e instalar aws-cli
check_tool aws || install_aws_cli

# Verificar e instalar helm
check_tool helm || {
  echo "Instalando Helm..."
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
}

# Verificar e instalar kubectl
check_tool kubectl || {
  echo "Instalando kubectl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install kubectl || brew link --overwrite kubernetes-cli
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
  fi
}

# Verificar e instalar eksctl
check_tool eksctl || {
  echo "Instalando eksctl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew tap weaveworks/tap
    brew install weaveworks/tap/eksctl
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
    sudo mv /tmp/eksctl /usr/local/bin
  fi
}

# Continuação do script após a verificação das ferramentas
echo "Todas as ferramentas necessárias estão instaladas."
echo "Procedendo com a configuração do cluster..."


# Definindo variáveis de ambiente
export K8S_VERSION="1.29"
export CLUSTER_NAME="api-whatsapp"
export AWS_DEFAULT_REGION="sa-east-1"
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"

# Verificando e criando o cluster EKS
if ! eksctl get cluster --name ${CLUSTER_NAME} --region ${AWS_DEFAULT_REGION} >/dev/null 2>&1; then
    echo "Criando o cluster ${CLUSTER_NAME}..."
    eksctl create cluster \
        --name ${CLUSTER_NAME} \
        --version ${K8S_VERSION} \
        --region ${AWS_DEFAULT_REGION} \
        --nodegroup-name node-autoscaling \
        --node-type t3.large \
        --nodes-min 1 \
        --nodes-max 10 \
        --asg-access
fi

# Associando provedor IAM OIDC
echo "Associando provedor IAM OIDC..."
eksctl utils associate-iam-oidc-provider --region ${AWS_DEFAULT_REGION} --cluster ${CLUSTER_NAME} --approve

# Preparando a política IAM para o Cluster Autoscaler
export AUTOSCALER_POLICY_FILE="/tmp/cluster-autoscaler-policy.json"
cat <<EOF > ${AUTOSCALER_POLICY_FILE}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions"
            ],
            "Resource": "*"
        }
    ]
}
EOF

# Criando a política IAM se ela não existir
POLICY_ARN=$(aws iam list-policies --query "Policies[?PolicyName=='ClusterAutoscalerPolicy'].Arn" --output text)
if [ -z "$POLICY_ARN" ]; then
    echo "Criando política IAM para Cluster Autoscaler..."
    POLICY_ARN=$(aws iam create-policy \
        --policy-name ClusterAutoscalerPolicy \
        --policy-document file://cluster-autoscaler-policy.json \
        --query 'Policy.Arn' \
        --output text)
else
    echo "Política IAM para Cluster Autoscaler já existe."
fi

# Criando uma service account para o Cluster Autoscaler com a política IAM
echo "Criando service account para Cluster Autoscaler..."
eksctl create iamserviceaccount \
    --region ${AWS_DEFAULT_REGION} \
    --name cluster-autoscaler \
    --namespace kube-system \
    --cluster ${CLUSTER_NAME} \
    --attach-policy-arn ${POLICY_ARN} \
    --override-existing-serviceaccounts \
    --approve

# Configurando repositório Helm para o Cluster Autoscaler e atualizando
echo "Configurando repositório Helm para Cluster Autoscaler..."
helm repo add autoscaler https://kubernetes.github.io/autoscaler
helm repo update

# Instalando o Cluster Autoscaler
echo "Instalando o Cluster Autoscaler..."
helm install cluster-autoscaler autoscaler/cluster-autoscaler \
    --namespace kube-system \
    --set autoDiscovery.clusterName=${CLUSTER_NAME} \
    --set awsRegion=${AWS_DEFAULT_REGION}

# Instalando o CSI driver para o EBS
eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
eksctl create iamserviceaccount \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster ${CLUSTER_NAME} \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole
eksctl create addon --name aws-ebs-csi-driver --cluster ${CLUSTER_NAME} --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole --force

# Instalando o RabbitMQ
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update       
helm install rabbitmq-cluster bitnami/rabbitmq \
  --set image.repository=edupoli/rabbitmq \
  --set image.tag=1.0.6 \
  --set auth.username=admin \
  --set auth.password=admin123 \
  --namespace infra --create-namespace

# Instalando mongoDB
echo "Instalando mongoDB..."
helm repo add mongodb https://mongodb.github.io/helm-charts
helm repo update
kubectl create namespace mongodb
kubectl label namespace mongodb monitoring=prometheus

# Instalando o MongoDB Community Operator
helm install community-operator mongodb/community-operator --namespace mongodb
kubectl wait --for=condition=available --timeout=600s deployment/mongodb-kubernetes-operator --namespace mongodb


# Cria o MongoDBCommunity e a Secret necessária
echo "Aplicando manifestos do MongoDB..."
kubectl apply -f - <<EOF
apiVersion: mongodbcommunity.mongodb.com/v1
kind: MongoDBCommunity
metadata:
  name: mongodb-replica-set
  namespace: mongodb
spec:
  members: 3
  type: ReplicaSet
  version: "6.0.5"
  security:
    authentication:
      modes: ["SCRAM"]
  users:
  - name: admin
    db: admin
    passwordSecretRef:
      name: mongodb-secret
    roles:
      - name: root
        db: admin
    scramCredentialsSecretName: my-scram
  additionalMongodConfig:
    storage.wiredTiger.engineConfig.journalCompressor: zlib
  statefulSet:
    spec:
      template:
        spec:
          containers:
            - name: mongod
              resources:
                limits:
                  cpu: "1"
                  memory: 2Gi
                requests:
                  cpu: 500m
                  memory: 1Gi
          affinity:
            podAntiAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
                - labelSelector:
                    matchExpressions:
                      - key: app
                        operator: In
                        values:
                          - mongodb-replica-set
                  topologyKey: "kubernetes.io/hostname"
      volumeClaimTemplates:
        - metadata:
            name: data-volume
          spec:
            accessModes:
              - ReadWriteOnce
            resources:
              requests:
                storage: 10Gi
---

apiVersion: v1
kind: Service
metadata:
  name: mongodb-replica-set-svc
  namespace: mongodb
spec:
  ports:
  - name: mongodb
    port: 27017
    protocol: TCP
    targetPort: 27017
  selector:
    app: mongodb-replica-set
  type: LoadBalancer 
---

apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
  namespace: mongodb
type: Opaque
stringData:
  password: admin123
EOF

# Espera até que todos os pods do MongoDB replica set estejam no estado Running
for pod in mongodb-replica-set-0 mongodb-replica-set-1 mongodb-replica-set-2; do
    while [[ $(kubectl get pod $pod -n mongodb -o 'jsonpath={.status.phase}') != "Running" ]]; do
        echo "Waiting for $pod to be ready..."
        sleep 10
    done
done

echo "Todos os pods do MongoDB replica set estão prontos."

# apply gitlab-registry
kubectl apply -f - <<EOF
apiVersion: v1
data:
  .dockerconfigjson: eyJhdXRocyI6eyJyZWdpc3RyeS5naXRsYWIuY29tIjp7InVzZXJuYW1lIjoiZWR1YXJkby1wb2xpY2FycG8iLCJwYXNzd29yZCI6ImdscGF0LXY0c0p6NUR5RTVRTlplTUY3YnJZIiwiZW1haWwiOiJlZHVhcmRvcG9saWNhcnBvQGdtYWlsLmNvbSIsImF1dGgiOiJaV1IxWVhKa2J5MXdiMnhwWTJGeWNHODZaMnh3WVhRdGRqUnpTbm8xUkhsRk5WRk9XbVZOUmpkaWNsaz0ifX19
kind: Secret
metadata:
  name: gitlab-registry
  namespace: default
type: kubernetes.io/dockerconfigjson
EOF

# apply jwt-secret
kubectl apply -f - <<EOF
apiVersion: v1
data:
  JWT_SECRET: NURFMjY3OTc3QTkyODVEQzdDQTZFNzZFM0JDNEE5RTcyQTdDQUI2M0VBNzkzMzhGNkJDNzQ0NDU4MkQxNjk4QUE0MTA0NTBGRUUwNUNFQjBCNTAzRDhFRDM5RkE3NTQwQThBMzFEMkJDNjVGNTk4MjczNEYwN0M2RDQzMzY4NEU=
kind: Secret
metadata:
  name: jwt-secret
  namespace: default
type: Opaque
EOF

# apply user-service-password
kubectl apply -f - <<EOF
apiVersion: v1
data:
  USER_SERVICE_PASSWORD: QGU1MDA2MUZGNDYxOTE0RDkyQjI5NTU2RTMxQjUzRjUxMEI2MEY4RjFBRkM0QzI1Rjg3NDk3QzA2OTk2Rjk5QzcyMEI5RDE2M0NENTcyOUIyNjJCNTM4MEI3ODI3RjE1M0JGREIwNTcxMEVDRjA4MDYwNzU1QjQwRjFDREZFN0VF
kind: Secret
metadata:
  name: user-service-password
  namespace: default
EOF


# apply mongodb-setup-job
kubectl apply -f - <<EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: mongodb-setup
  namespace: mongodb
spec:
  template:
    spec:
      containers:
      - name: mongodb-setup
        image: mongo:6.0.5
        env:
        - name: MONGO_INITDB_ROOT_USERNAME
          value: admin
        - name: MONGO_INITDB_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mongodb-secret
              key: password
        command: ["/bin/bash"]
        args:
        - "-c"
        - |
        
          mongosh --host mongodb-replica-set-svc.mongodb.svc.cluster.local --authenticationDatabase admin -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
          use wappi;

          db.createUser({
            user: "admin",
            pwd: "admin123",
            roles: [
              { role: "dbOwner", db: "wappi" },
              { role: "readWrite", db: "wappi" }
            ]
          });

          db.plans.insertMany([
            {
              "name": "EXPERIMENTAL",
              "maxInstances": 1,
              "price": 0,
              "expiresIn": 2,
              "isActive": true,
              "description": "Este plano permite testar a API por 2 dias.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "MENSAL",
              "maxInstances": 5,
              "price": 350,
              "expiresIn": 30,
              "isActive": true,
              "description": "Este plano oferece até 5 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "TRIMESTRAL",
              "maxInstances": 10,
              "price": 800,
              "expiresIn": 90,
              "isActive": true,
              "description": "Este plano oferece até 10 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "SEMESTRAL",
              "maxInstances": 30,
              "price": 1200,
              "expiresIn": 180,
              "isActive": true,
              "description": "Este plano oferece até 30 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "ANUAL",
              "maxInstances": 60,
              "price": 2500,
              "expiresIn": 365,
              "isActive": true,
              "description": "Este plano oferece até 60 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            }
          ]);

          // Aguardar a propagação dos dados antes de consultar
          sleep(5000);

          var planId = db.plans.findOne({name: "ANUAL"})._id;
          db.users.insertOne({
            "name": "System User",
            "phone": "4333754246",
            "email": "eduardo@gmail.com",
            "username": "system",
            "password": "@e50061FF461914D92B29556E31B53F510B60F8F1AFC4C25F87497C06996F99C720B9D163CD5729B262B5380B7827F153BFDB05710ECF08060755B40F1CDFE7EE4",
            "isActive": true,
            "planId": planId,
            "instances": [],
            "expiresIn": new Date("2099-02-28T13:13:44.675Z"),
            "isAdmin": true
          });
          EOF
      restartPolicy: OnFailure
EOF


# deploy user-service
kubectl apply -f - <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: user-service
spec:
  replicas: 2
  selector:
    matchLabels:
      app: user-service
  template:
    metadata:
      labels:
        app: user-service
    spec:
      containers:
      - name: user-service-app
        image: registry.gitlab.com/worker-group/users-service:latest
        resources:
          limits:
            cpu: '600m'
            memory: '3Gi'
          requests:
            cpu: '250m'
            memory: '1Gi'
        env:
          - name: NODE_ENV
            value: production
          - name: PORT
            value: '3000'
          - name: HOST
            value: '0.0.0.0'
          - name: MONGO_URI
            value: 'mongodb+srv://admin:admin123@mongodb-replica-set-svc.mongodb.svc.cluster.local/wappi?replicaSet=mongodb-replica-set&ssl=false'
          - name: JWT_SECRET
            valueFrom:
              secretKeyRef:
                name: jwt-secret
                key: JWT_SECRET
        livenessProbe:
          httpGet:
            path: /health
            port: 3000
          initialDelaySeconds: 15
          timeoutSeconds: 2
        readinessProbe:
          httpGet:
            path: /health
            port: 3000
          initialDelaySeconds: 5
          timeoutSeconds: 2
      imagePullSecrets:
        - name: gitlab-registry
---

apiVersion: v1
kind: Service
metadata:
  name: user-service-svc
spec:
  selector:
    app: user-service
  ports:
  - name: http
    protocol: TCP
    port: 3000
    targetPort: 3000
  type: ClusterIP
EOF


# deploy api-server-config
kubectl apply -f - <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: api-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: api-server
  template:
    metadata:
      labels:
        app: api-server
    spec:
      containers:
      - name: api-server-app
        image: registry.gitlab.com/worker-group/api-service:latest
        resources:
          limits:
            cpu: '600m'
            memory: '3Gi'
          requests:
            cpu: '250m'
            memory: '1Gi'
        envFrom:
          - configMapRef:
              name: api-server-config
        env:
          - name: JWT_SECRET
            valueFrom:
              secretKeyRef:
                name: jwt-secret
                key: JWT_SECRET
          - name: USER_SERVICE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: user-service-password
                key: USER_SERVICE_PASSWORD
        livenessProbe:
          httpGet:
            path: /health
            port: 3000
          initialDelaySeconds: 15
          timeoutSeconds: 2
        readinessProbe:
          httpGet:
            path: /health
            port: 3000
          initialDelaySeconds: 5
          timeoutSeconds: 2
      imagePullSecrets:
        - name: gitlab-registry

---
apiVersion: v1
kind: Service
metadata:
  name: api-server-service
spec:
  selector:
    app: api-server
  ports:
  - name: http
    protocol: TCP
    port: 3000
    targetPort: 3000
  type: ClusterIP

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: api-server-config
data:
  NODE_ENV: production
  PORT: '3000'
  HOST: '0.0.0.0'
  AMQP_HOST: rabbitmq-cluster-headless.infra.svc.cluster.local
  AMQP_PORT: '5672'
  AMQP_USER: admin
  AMQP_PASSWORD: admin
  AMQP_EXCHANGE: API-WhatsApp
  USER_SERVICE_URL: http://user-service-svc.default.svc.cluster.local:3000
EOF


echo "Script concluído."

#!/bin/bash

# Função para verificar se uma ferramenta está instalada
check_tool_installed() {
    if ! command -v $1 &> /dev/null
    then
        echo "$1 não está instalado."
        return 1
    else
        echo "$1 já está instalado."
        return 0
    fi
}


# Função para verificar e configurar o AWS CLI
check_configure_aws_cli() {
    check_tool_installed aws
    if [ $? -eq 1 ]; then
        echo "Instalando o AWS CLI..."
        if [[ "$OSTYPE" == "darwin"* ]]; then
            # macOS
            curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
            sudo installer -pkg AWSCLIV2.pkg -target /
        elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
            # Linux
            curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
            unzip awscliv2.zip
            sudo ./aws/install
        else
            echo "Sistema operacional não suportado."
            exit 1
        fi

  echo "AWS CLI instalado com sucesso."
  echo "Por favor, crie sua ACCESS KEY e SECRET KEY acessando o serviço IAM em: https://console.aws.amazon.com/iam/"
  echo "Após ter os dados, execute o comando 'aws configure' no terminal e preencha os dados."
  echo "Rode este script novamente após configurar as credenciais."
  exit 0
    else
        echo "Verificando se as credenciais da AWS estão configuradas..."
        if ! aws sts get-caller-identity &> /dev/null; then
            echo "Credenciais da AWS não configuradas ou inválidas."
            echo "Por favor, configure suas credenciais da AWS executando 'aws configure'."
            exit 1
        else
            echo "Credenciais da AWS verificadas com sucesso."
        fi
    fi
}

# Verifica e configura o AWS CLI
check_configure_aws_cli



# Função para verificar se uma ferramenta está instalada
check_tool() {
  if ! command -v $1 &> /dev/null; then
    echo "$1 não está instalado."
    return 1
  else
    echo "$1 já está instalado."
    return 0
  fi
}

# Verificar e instalar aws-cli
check_tool aws || install_aws_cli

# Verificar e instalar helm
check_tool helm || {
  echo "Instalando Helm..."
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
}

# Verificar e instalar kubectl
check_tool kubectl || {
  echo "Instalando kubectl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install kubectl || brew link --overwrite kubernetes-cli
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
  fi
}

# Verificar e instalar eksctl
check_tool eksctl || {
  echo "Instalando eksctl..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew tap weaveworks/tap
    brew install weaveworks/tap/eksctl
  elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
    sudo mv /tmp/eksctl /usr/local/bin
  fi
}

# Continuação do script após a verificação das ferramentas
echo "Todas as ferramentas necessárias estão instaladas."
echo "Procedendo com a configuração do cluster..."


export KARPENTER_NAMESPACE="kube-system"
export KARPENTER_VERSION="0.35.2"
export K8S_VERSION="1.29"
export AWS_PARTITION="aws"
export CLUSTER_NAME="api-smille"
export AWS_DEFAULT_REGION="us-east-2"
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
export TEMPOUT="$(mktemp)"

curl -fsSL https://raw.githubusercontent.com/aws/karpenter-provider-aws/v"${KARPENTER_VERSION}"/website/content/en/preview/getting-started/getting-started-with-karpenter/cloudformation.yaml  > "${TEMPOUT}" \
&& aws cloudformation deploy \
  --stack-name "Karpenter-${CLUSTER_NAME}" \
  --template-file "${TEMPOUT}" \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides "ClusterName=${CLUSTER_NAME}"

eksctl create cluster -f - <<EOF
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig
metadata:
  name: ${CLUSTER_NAME}
  region: ${AWS_DEFAULT_REGION}
  version: "${K8S_VERSION}"
  tags:
    karpenter.sh/discovery: ${CLUSTER_NAME}

iam:
  withOIDC: true
  podIdentityAssociations:
  - namespace: "${KARPENTER_NAMESPACE}"
    serviceAccountName: karpenter
    roleName: ${CLUSTER_NAME}-karpenter
    permissionPolicyARNs:
    - arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:policy/KarpenterControllerPolicy-${CLUSTER_NAME}

iamIdentityMappings:
- arn: "arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/KarpenterNodeRole-${CLUSTER_NAME}"
  username: system:node:{{EC2PrivateDNSName}}
  groups:
  - system:bootstrappers
  - system:nodes

managedNodeGroups:
- instanceType: m5.large
  amiFamily: AmazonLinux2
  name: ${CLUSTER_NAME}-ng
  desiredCapacity: 2
  minSize: 1
  maxSize: 10

addons:
- name: eks-pod-identity-agent
EOF

export CLUSTER_ENDPOINT="$(aws eks describe-cluster --name "${CLUSTER_NAME}" --query "cluster.endpoint" --output text)"
export KARPENTER_IAM_ROLE_ARN="arn:${AWS_PARTITION}:iam::${AWS_ACCOUNT_ID}:role/${CLUSTER_NAME}-karpenter"

echo "${CLUSTER_ENDPOINT} ${KARPENTER_IAM_ROLE_ARN}"

helm repo add karpenter https://charts.karpenter.sh/
helm repo update

aws iam create-service-linked-role --aws-service-name spot.amazonaws.com > /dev/null 2>&1 || true

# Instalando o Karpenter
helm registry logout public.ecr.aws

helm upgrade --install karpenter oci://public.ecr.aws/karpenter/karpenter --version "${KARPENTER_VERSION}" --namespace "${KARPENTER_NAMESPACE}" --create-namespace \
  --set "settings.clusterName=${CLUSTER_NAME}" \
  --set "settings.interruptionQueue=${CLUSTER_NAME}" \
  --set controller.resources.requests.cpu=1 \
  --set controller.resources.requests.memory=1Gi \
  --set controller.resources.limits.cpu=1 \
  --set controller.resources.limits.memory=1Gi \
  --wait


cat <<EOF | envsubst | kubectl apply -f -
apiVersion: karpenter.sh/v1beta1
kind: NodePool
metadata:
  name: default
spec:
  template:
    spec:
      requirements:
        - key: "kubernetes.io/arch"
          operator: In
          values: ["amd64"]
        - key: "kubernetes.io/os"
          operator: In
          values: ["linux"]
        - key: "karpenter.sh/capacity-type"
          operator: In
          values: ["on-demand"]
        - key: "topology.kubernetes.io/zone"
          operator: In
          values: ["${AWS_DEFAULT_REGION}a", "${AWS_DEFAULT_REGION}b"]
      nodeClassRef:
        name: default
  disruption:
    consolidationPolicy: WhenUnderutilized
    expireAfter: 720h # 30 * 24h = 720h

---
apiVersion: karpenter.k8s.aws/v1beta1
kind: EC2NodeClass
metadata:
  name: default
spec:
  amiFamily: AL2 # Amazon Linux 2
  role: "KarpenterNodeRole-${CLUSTER_NAME}"
  subnetSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}"
  securityGroupSelectorTerms:
    - tags:
        karpenter.sh/discovery: "${CLUSTER_NAME}"
EOF
eksctl utils update-cluster-logging --enable-types=all --approve --region=${AWS_DEFAULT_REGION} --cluster=${CLUSTER_NAME}

# Instalando o CSI driver para o EBS
eksctl utils associate-iam-oidc-provider --cluster ${CLUSTER_NAME} --approve
eksctl create iamserviceaccount \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster ${CLUSTER_NAME} \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole
eksctl create addon --name aws-ebs-csi-driver --cluster ${CLUSTER_NAME} --service-account-role-arn arn:aws:iam::${AWS_ACCOUNT_ID}:role/AmazonEKS_EBS_CSI_DriverRole --force

# Instalando mongoDB
echo "Instalando mongoDB..."
helm repo add mongodb https://mongodb.github.io/helm-charts
helm repo update
kubectl create namespace mongodb
kubectl label namespace mongodb monitoring=prometheus

# Instalando o MongoDB Community Operator
helm install community-operator mongodb/community-operator --namespace mongodb
kubectl wait --for=condition=available --timeout=600s deployment/mongodb-kubernetes-operator --namespace mongodb


# Cria o MongoDBCommunity e a Secret necessária
echo "Aplicando manifestos do MongoDB..."
kubectl apply -f - <<EOF
apiVersion: mongodbcommunity.mongodb.com/v1
kind: MongoDBCommunity
metadata:
  name: mongodb-replica-set
  namespace: mongodb
spec:
  members: 3
  type: ReplicaSet
  version: "6.0.5"
  security:
    authentication:
      modes: ["SCRAM"]
  users:
  - name: admin
    db: admin
    passwordSecretRef:
      name: mongodb-secret
    roles:
      - name: root
        db: admin
    scramCredentialsSecretName: my-scram
  additionalMongodConfig:
    storage.wiredTiger.engineConfig.journalCompressor: zlib
  statefulSet:
    spec:
      template:
        spec:
          containers:
            - name: mongod
              resources:
                limits:
                  cpu: "1"
                  memory: 2Gi
                requests:
                  cpu: 500m
                  memory: 1Gi
          affinity:
            podAntiAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
                - labelSelector:
                    matchExpressions:
                      - key: app
                        operator: In
                        values:
                          - mongodb-replica-set
                  topologyKey: "kubernetes.io/hostname"
      volumeClaimTemplates:
        - metadata:
            name: data-volume
          spec:
            accessModes:
              - ReadWriteOnce
            resources:
              requests:
                storage: 10Gi
---
apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
  namespace: mongodb
type: Opaque
stringData:
  password: admin123
EOF

while [[ $(kubectl get MongoDBCommunity mongodb-replica-set -n mongodb -o 'jsonpath={..status.phase}') != "Running" ]]; do
  echo "Waiting for MongoDB replica set to be ready..."
  sleep 10
done


echo "Script concluído."



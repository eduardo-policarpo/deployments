#!/bin/bash

# Executa o script com o tempo de autenticação prolongado
sudo -v
manter_autenticacao_sudo() {
  while true; do
    sudo -v
    sleep 60
  done
}

# Iniciar o processo em segundo plano para manter a autenticação do sudo
manter_autenticacao_sudo &

spinner() {
  local pid=$1
  local delay=0.1
  local spinstr='/'
  echo -n "Aguarde..."
  while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
    printf " [%c]  " "$spinstr" | lolcat
    sleep $delay
    printf "\b\b\b\b\b\b\b\b\b"
  done
  printf "        \b\b\b\b\b\b\b\b"
}

sudo apt install -y apt-transport-https ca-certificates curl software-properties-common jq
sudo snap install lolcat

get_public_ip() {
  local ip services=(https://ipinfo.io/ip http://ifconfig.me)

  for service in "${services[@]}"; do
    ip=$(curl -s "$service")
    if [ -n "$ip" ]; then
      echo "$ip"
      return
    fi
  done

  echo "$(hostname -I | awk '{print $1}')"
}

validate_domain() {
  local host=$1 ip vps_ip=$(get_public_ip)

  ip=$(dig +short "$host" | tail -n1)

  if [ -n "$ip" ] && [ "$ip" == "$vps_ip" ]; then
    return 0
  elif whiptail --title "ERRO" --yesno "O domínio informado não existe ou não aponta para essa VPS. Deseja informar um novo domínio?" --fb 10 60; then
    return 1
  else
    exit 1
  fi
}

while true; do
  export DOMAIN=$(whiptail --title "INFORMAR DOMINIO para a API" --inputbox "Digite o valor:" --fb 10 60 3>&1 1>&2 2>&3)
  exitstatus=$?

  if [ $exitstatus -ne 0 ]; then
    exit 1
  fi

  if [ -z "$DOMAIN" ]; then
    whiptail --title "Erro" --msgbox "O dominio e obrigatorio...Você deve informar um valor." --fb 10 60
    continue
  fi

  if validate_domain "$DOMAIN"; then
    if (whiptail --title "DOMINIO" --yesno "$DOMAIN está correto?" --fb 10 60 3>&1 1>&2 2>&3); then
      break
    fi
  fi
done

# Função para instalar Docker
install_docker() {
  if ! command -v docker &>/dev/null; then
    echo "Docker não encontrado. Instalando Docker..."
    sudo apt update
    if [ ! -f /usr/share/keyrings/docker-archive-keyring.gpg ]; then
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
    sudo apt update
    sudo apt install -y docker-ce docker-ce-cli containerd.io
    sudo usermod -aG docker "$USER"
    sudo systemctl enable docker
    sudo systemctl start docker
    docker --version
    sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    docker-compose --version
    echo "O Docker e o Docker Compose foram instalados e configurados com sucesso!"
  else
    echo "Docker já está instalado."
  fi
}

# Função para instalar Kind
install_kind() {
  if ! command -v kind &>/dev/null; then
    echo "Kind não encontrado. Instalando Kind..."
    [ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
    chmod +x ./kind
    sudo mv ./kind /usr/local/bin/kind

    echo "Kind foi instalado com sucesso!"
  else
    echo "Kind já está instalado."
  fi
}

# Função para instalar Kubectl
install_kubectl() {
  if ! command -v kubectl &>/dev/null; then
    echo "Kubectl não encontrado. Instalando Kubectl..."
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /usr/share/keyrings/kubernetes-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list >/dev/null
    sudo apt-get update
    sudo apt-get install -y kubectl
    echo "Kubectl foi instalado com sucesso!"
  else
    echo "Kubectl já está instalado."
  fi
}

# Função para instalar Helm
install_helm() {
  if ! command -v helm &>/dev/null; then
    echo "Helm não encontrado. Instalando Helm..."
    curl -fsSL -o helm-signing.asc https://baltocdn.com/helm/signing.asc
    sudo cp helm-signing.asc /etc/apt/trusted.gpg.d/
    rm helm-signing.asc
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install -y helm
    echo "Helm foi instalado com sucesso!"
  else
    echo "Helm já está instalado."
  fi
}

# Função para criar o cluster Kind
create_kind_cluster() {
  if ! kind get clusters | grep -q api-server; then
    echo "Criando o cluster api-server..."
    kind create cluster --name api-server --config - <<EOF
apiVersion: kind.x-k8s.io/v1alpha4
kind: Cluster
networking:
  apiServerAddress: "0.0.0.0"
  apiServerPort: 6443
nodes:
  - role: control-plane
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 80
        listenAddress: 0.0.0.0
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        listenAddress: 0.0.0.0
        protocol: TCP
  - role: worker
  - role: worker
EOF

    # habilitando metrics no kind
    kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml
    kubectl patch deployment metrics-server -n kube-system --patch "$(
      cat <<EOF
spec:
  template:
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls
        name: metrics-server
EOF
    )"

    echo "Cluster api-server criado com sucesso."
  else
    echo "Cluster api-server já existe."
  fi
}

# Aguarde até que o cluster Kind esteja pronto
wait_for_kind_cluster_ready() {
  echo "Aguardando o cluster Kind ficar pronto..."
  until kubectl get nodes &>/dev/null; do
    sleep 1
  done
  echo "Cluster Kind está pronto."
}

wait_for_metallb_webhook_ready() {
  echo "Aguardando o webhook do MetalLB ficar pronto..."
  until kubectl rollout status deployment/controller -n metallb-system | grep "successfully rolled out"; do
    sleep 1
  done
  echo "Webhook do MetalLB está pronto."
}

install_docker
install_kind
install_kubectl
install_helm
create_kind_cluster
wait_for_kind_cluster_ready &
spinner $!

helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

helm install mongodb-server bitnami/mongodb \
  --set auth.username=admin \
  --set auth.password=admin \
  --set auth.database=wappi \
  --set service.type=LoadBalancer

helm install rabbitmq-server bitnami/rabbitmq \
  --set image.repository=edupoli/rabbitmq \
  --set image.tag=1.0.6 \
  --set auth.username=admin \
  --set auth.password=admin \
  --set service.type=LoadBalancer

# Configuração do cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl create namespace cert-manager || true &
spinner $!
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.5.0 --set installCRDs=true &
spinner $!
kubectl wait --namespace cert-manager --for=condition=ready pod --selector=app.kubernetes.io/name=cert-manager --timeout=90s &
spinner $!
kubectl apply -f https://raw.githubusercontent.com/edupoli/whatsapp-api/master/gitlab.yaml &
spinner $!

# deploy api-server
curl -s https://raw.githubusercontent.com/edupoli/whatsapp-api/master/api-service.yaml >api-server.yaml
envsubst <api-server.yaml | kubectl apply -f -
kubectl wait --for=condition=ready pod -l app=api-server --timeout=300s &
spinner $!
rm api-server.yaml

# Instalação do NGINX Ingress Controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=90s &
spinner $!
kubectl patch svc ingress-nginx-controller -n ingress-nginx -p '{"spec": {"type": "LoadBalancer"}}'

# Configuração do ClusterIssuer
kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: seu-email@dominio.com
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class: nginx
EOF

# Configuração do Ingress
kubectl apply -f - <<EOF
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: meu-ingress
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  ingressClassName: nginx
  rules:
  - host: $DOMAIN
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: api-server-service
            port:
              number: 3000
  tls:
  - hosts:
    - $DOMAIN
    secretName: meu-ingress-tls
EOF

# Get the subnet information from the kind Docker network
subnet=$(docker network inspect kind -f '{{(index .IPAM.Config 0).Subnet}}')
IFS='/' read -ra ADDR <<<"$subnet"
network="${ADDR[0]}"
mask="${ADDR[1]}"

# Calculate the range of IPs for MetalLB
IFS='.' read -ra ADDR <<<"$network"
ip_base="${ADDR[0]}.${ADDR[1]}.${ADDR[2]}."
start_ip="${ip_base}200"
end_ip="${ip_base}250"

# Instalar MetalLB
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml

# Aguarde até que o webhook do MetalLB esteja pronto
wait_for_metallb_webhook_ready &
spinner $!

# Aplicar a configuração do MetalLB
cat <<EOF | kubectl apply -f -
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: metallb-native
  namespace: metallb-system
spec:
  addresses:
  - $start_ip-$end_ip
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: empty
  namespace: metallb-system
EOF

#!/bin/bash

# Endereço IP externo conhecido para a verificação da rota
EXTERNAL_IP_TO_TEST="1.1.1.1"

# Obtenha a interface de rede e o IP local usados para alcançar o IP externo conhecido
NETWORK_INFO=$(ip route get $EXTERNAL_IP_TO_TEST)
LOCAL_IP=$(echo $NETWORK_INFO | grep -oP 'src \K\S+')
INTERFACE=$(echo $NETWORK_INFO | grep -oP 'dev \K\S+')

# Obtenha o subnet mask para o IP local
SUBNET_INFO=$(ip -o -f inet addr show $INTERFACE | awk '{print $4}')
SUBNET_MASK=$(echo $SUBNET_INFO | cut -d/ -f2)

# Calcula o range de IP para o MetalLB usando o IP local e a máscara de rede
IFS=. read -r i1 i2 i3 i4 <<<$LOCAL_IP
NETWORK_BASE="$i1.$i2.$i3.0/$SUBNET_MASK"

# Range de IPs para MetalLB (Modifique conforme necessário)
METALLB_RANGE_START="$i1.$i2.$i3.100"
METALLB_RANGE_END="$i1.$i2.$i3.200"

# Exibe as informações
echo "Interface de rede com acesso externo: $INTERFACE"
echo "IP local na interface: $LOCAL_IP"
echo "Range de rede: $NETWORK_BASE"
echo "Range sugerido para MetalLB: $METALLB_RANGE_START - $METALLB_RANGE_END"

# Saída opcional: Configuração de range para uso no MetalLB ConfigMap
echo "ConfigMap para MetalLB:"
cat <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - $METALLB_RANGE_START-$METALLB_RANGE_END
EOF

#!/bin/bash

# Função para calcular o range de IPs para o MetalLB
calculate_metallb_range() {
  local EXTERNAL_IP_TO_TEST="1.1.1.1" # IP externo para teste
  local NETWORK_INFO=$(ip route get $EXTERNAL_IP_TO_TEST)
  local LOCAL_IP=$(echo $NETWORK_INFO | grep -oP 'src \K\S+')
  local INTERFACE=$(echo $NETWORK_INFO | grep -oP 'dev \K\S+')
  local SUBNET_INFO=$(ip -o -f inet addr show $INTERFACE | awk '{print $4}')
  local SUBNET_MASK=$(echo $SUBNET_INFO | cut -d/ -f2)

  # Cálculo do range IP baseando-se no terceiro octeto
  IFS=. read -r i1 i2 i3 i4 <<<$LOCAL_IP
  local range_start="$i1.$i2.$i3.100"
  local range_end="$i1.$i2.$i3.200"

  # Configura as variáveis de ambiente com o range calculado
  export METALLB_RANGE_START=$range_start
  export METALLB_RANGE_END=$range_end
}

# Chama a função para calcular o range
calculate_metallb_range

# Agora pode utilizar as variáveis METALLB_RANGE_START e METALLB_RANGE_END
echo "Aplicando configuração do MetalLB com o range $METALLB_RANGE_START-$METALLB_RANGE_END"
microk8s enable metallb:$METALLB_RANGE_START-$METALLB_RANGE_END

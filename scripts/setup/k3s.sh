# #!/bin/bash

# # Definição de cores para saída no terminal
# readonly RED="\033[1;31m"
# readonly WHITE="\033[1;37m"
# readonly GREEN="\033[1;32m"
# readonly GRAY_LIGHT="\033[0;37m"
# readonly YELLOW="\033[1;33m"
# readonly CYAN="\033[1;36m"
# readonly NC="\033[0m" # Sem cor

# # Definição de variáveis globais
# export DOMAIN
# export APP_URL
# export API_URL
# export USERS_URL
# export MONGODB_ROOT_PASSWORD

# # Verifica se o script está sendo executado como root
# if [ "$EUID" -ne 0 ]; then
#   printf "${YELLOW}⚠️ Este script deve ser executado como root. Use 'sudo ./setup.sh${NC}\n"
#   exit 1
# fi

# apt update

# # Função para instalar Helm
# install_helm() {
#   if ! command -v helm &>/dev/null; then
#     printf "${CYAN}⚠️  Helm não encontrado. Instalando Helm...${NC}\n"
#     if [[ "$(uname -s)" == "Darwin" ]]; then
#       brew install helm
#     else
#       sudo snap install helm --classic
#     fi
#     printf "${GREEN}✔️ Helm foi instalado com sucesso!${NC}\n"
#   else
#     printf "${YELLOW}⚠️  Helm já está instalado!${NC}\n"
#   fi
# }

# # Instalação do K3s
# curl -sfL https://get.k3s.io | sh -
# export KUBECONFIG=/etc/rancher/k3s/k3s.yaml



# while true; do
#   printf "${WHITE}💻 Insira o domínio para a API: ${GRAY_LIGHT} "
#   read -r DOMAIN

#   # Verifica se um valor foi fornecido
#   if [ -z "$DOMAIN" ]; then
#     printf "${YELLOW}❕ O domínio é obrigatório. Você deve informar um valor.❕ ${GRAY_LIGHT}\n"
#     continue  # Continua solicitando o domínio se nenhum valor foi fornecido
#   else
#     break  # Sai do loop se um valor foi fornecido
#   fi
# done

# printf "${GREEN}✔ Domínio recebido: $DOMAIN ${GRAY_LIGHT}\n"



# # Atribuicao de valores as variaveis
# APP_URL="${HTTP_PROTOCOL}://${DOMAIN}"
# API_URL="${HTTP_PROTOCOL}://api.${DOMAIN}"
# USERS_URL="${HTTP_PROTOCOL}://users.${DOMAIN}"



# # Solicitar senha do MongoDB com validação e confirmação
# while true; do
#   printf "${WHITE}Digite a senha para o root do MongoDB (não pode ser vazia):${GRAY_LIGHT} "
#   read -r -s mongodb_password
#   printf "\n${WHITE}Confirme a senha:${GRAY_LIGHT} "
#   read -r -s mongodb_password_confirm
#   printf "\n"

#   if [[ -z "$mongodb_password" || -z "$mongodb_password_confirm" ]]; then
#     printf "${RED}Erro: A senha não pode ser vazia.${NC}\n"
#   elif [[ "$mongodb_password" != "$mongodb_password_confirm" ]]; then
#     printf "${RED}Erro: As senhas não coincidem.${NC}\n"
#   else
#     printf "${GREEN}Senha do MongoDB definida.${NC}\n"
#     break
#   fi
# done

# MONGODB_ROOT_PASSWORD=$mongodb_password
# install_helm

# helm repo add jetstack https://charts.jetstack.io
# helm repo add bitnami https://charts.bitnami.com/bitnami
# helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
# helm repo update

# # Instala o Prometheus
# helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace

# # deploy rabbitmq
# helm install rabbitmq-cluster bitnami/rabbitmq \
#   --set communityPlugins="https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.12.0/rabbitmq_delayed_message_exchange-3.12.0.ez" \
#   --set extraPlugins="rabbitmq_delayed_message_exchange" \
#   --set replicaCount=1 \
#   --set auth.username=admin \
#   --set auth.password=admin \
#   --set auth.securePassword=false \
#   --set persistence.enabled=true \
#   --set persistence.size=5Gi \
#   --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
#   --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
#   --set service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
#   --set service.type=NodePort \
#   --set service.nodePorts.amqp=30072 \
#   --set service.nodePorts.management=30073 \
#   --namespace infra --create-namespace

# # Aguardar os pods do RabbitMQ  estarem prontos
# printf "${YELLOW}⚠️  Aguardando RabbitNQM ficar com Status Ready...${NC}\n"
# kubectl rollout status statefulset/rabbitmq-cluster -n infra --timeout=600s


# # deploy redis
# helm install redis-server bitnami/redis \
#   --set master.persistence.size=2Gi \
#   --set architecture="replication" \
#   --set replicaCount=1 \
#   --set master.service.type=NodePort \
#   --set master.service.nodePorts.redis=30007 \
#   --set master.service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
#   --set auth.enabled=false \
#   --set resources.limits.cpu=500m,resources.limits.memory=512Mi \
#   --set resources.requests.cpu=250m,resources.requests.memory=256Mi \
#   --namespace infra --create-namespace

# # Aguardar os pods do Redis  estarem prontos
# printf "${YELLOW}⚠️  Aguardando Redis ficar com Status Ready...${NC}\n"
# kubectl rollout status statefulset/redis-server-master -n infra --timeout=600s
# kubectl rollout status statefulset/redis-server-replicas -n infra --timeout=600s


# # deploy mongodb
# helm install mongodb-standalone bitnami/mongodb \
#   --set image.repository=bitnami/mongodb \
#   --set image.tag=7.0.11 \
#   --set image.pullPolicy=Always \
#   --set architecture="standalone" \
#   --set auth.enabled=true \
#   --set auth.rootPassword=$MONGODB_ROOT_PASSWORD \
#   --set auth.usernames={admin} \
#   --set auth.passwords={admin} \
#   --set auth.databases={wappi} \
#   --set externalAccess.autoDiscovery.resourcesPreset="medium" \
#   --set service.type=NodePort \
#   --set service.externalIPs[0]=$(hostname -I | awk '{print $1}') \
#   --set service.nodePorts.mongodb=30676 \
#   --set externalAccess.service.nodePorts[0]=30676 \
#   --set externalAccess.service.type=NodePort \
#   --namespace infra --create-namespace

# # Aguardar os pods do MongoDB estarem prontos
# printf "${YELLOW}⚠️  Aguardando MongoDB ficar com Status Ready...${NC}\n"
# kubectl rollout status deployment/mongodb-standalone -n infra --timeout=600s


# # # Aplicar o job para configurar o MongoDB
# # printf "${GREEN}✔️ Aplicando o job de configuração do MongoDB...${NC}\n"
# # kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/mongodb/mongodb-setup-job.yaml?ref_type=heads
# # printf "${YELLOW}⚠️  Verificando o status do job de configuração do MongoDB...${NC}\n"
# # kubectl wait --for=condition=complete job/mongodb-setup -n infra --timeout=300s

# # # aplicar secrets
# # printf "${GREEN}✔️ Aplicando as secrets no cluster...${NC}\n"
# # kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/gitlab-registry.yaml?ref_type=heads
# # kubectl apply -f https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/secrets/jwt-secret.yaml?ref_type=heads

# # # deploy client-service
# # printf "${GREEN}✔️ Deploy e configuracao da aplicacao client-service...${NC}\n"
# # curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/client-service-dev.yaml?ref_type=heads >client-service-dev.yaml
# # envsubst <client-service-dev.yaml | kubectl apply -f -
# # kubectl wait --for=condition=ready pod -l app=client-service --timeout=300s

# # # deploy api-server
# # printf "${GREEN}✔️ Deploy e configuracao da aplicacao api-server...${NC}\n"
# # curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/api-service-dev.yaml?ref_type=heads >api-service-dev.yaml
# # export APP_URL="${API_URL}"
# # envsubst <api-service-dev.yaml | kubectl apply -f -
# # kubectl wait --for=condition=ready pod -l app=api-service --timeout=300s

# # # deploy user-service
# # printf "${GREEN}✔️ Deploy e configuracao da aplicacao user-service...${NC}\n"
# # curl -s https://gitlab.com/eduardo-policarpo/deployments/-/raw/main/manifests/development/user-service-dev.yaml?ref_type=heads >user-service-dev.yaml
# # export APP_URL="${USERS_URL}"
# # envsubst <user-service-dev.yaml | kubectl apply -f -
# # kubectl wait --for=condition=ready pod -l app=user-service --timeout=300s

# # # deploy ingress controller
# # printf "${GREEN}✔️ Deploy e configuracao do ingress controller...${NC}\n"
# # kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
# # kubectl wait --namespace ingress-nginx \
# #   --for=condition=ready pod \
# #   --selector=app.kubernetes.io/component=controller \
# #   --timeout=180s

# # # Esperar pelo webhook de admissão ficar pronto
# # while ! kubectl get --raw "/apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations/ingress-nginx-admission" &>/dev/null; do
# #   printf "${YELLOW}⚠️  Aguardando o webhook de admissão do Ingress ficar com Status Ready...${NC}\n"
# #   sleep 10
# # done


#!/bin/bash

# Definição de cores para saída no terminal
readonly RED="\033[1;31m"
readonly WHITE="\033[1;37m"
readonly GREEN="\033[1;32m"
readonly GRAY_LIGHT="\033[0;37m"
readonly YELLOW="\033[1;33m"
readonly NC="\033[0m" # Sem cor

# Definição de variáveis globais
export DOMAIN
export HTTP_PROTOCOL="https"
export APP_URL
export API_URL
export USERS_URL
export MONGODB_ROOT_PASSWORD

# Função para obter o IP público
get_public_ip() {
  local ip
  local services=(
    "https://api.ipify.org"
    "https://ipinfo.io/ip"
    "http://ifconfig.me"
    "https://ipecho.net/plain"
    "https://ifconfig.co"
    "https://myexternalip.com/raw"
  )

  for service in "${services[@]}"; do
    ip=$(timeout 5 curl -s "$service")
    if [[ "$ip" =~ ^[0-9]{1,3}(\.[0-9]{1,3}){3}$ ]]; then
      echo "$ip"
      return 0
    fi
  done

  printf "${RED}❌  Não foi possível determinar o IP público.${GRAY_LIGHT}\n"
  exit 1
}

# Função para validar se o domínio aponta para o IP público correto
validate_domain() {
  local host=$1
  local vps_ip=$(get_public_ip)
  local ip=$(dig +short A "$host")

  if [ -z "$ip" ]; then
    return 1
  elif [ "$ip" == "$vps_ip" ]; then
    return 0
  else
    return 2
  fi
}


# Função para adicionar o domínio e subdomínios ao arquivo /etc/hosts
add_to_hosts() {
  local ip=$(hostname -I | awk '{print $1}')
  local domain=$1
  local subdomains=("api" "users")
  local domains=("$domain")

  # Adicionar subdomínios à lista de domínios
  for sub in "${subdomains[@]}"; do
    domains+=("$sub.$domain")
  done

  # Adicionar domínios ao /etc/hosts
  for d in "${domains[@]}"; do
    local host_entry="$ip $d"
    if ! grep -q "$host_entry" /etc/hosts; então
      echo "$host_entry" | sudo tee -a /etc/hosts >/dev/null
      printf "${GREEN}✔️  Domínio $d adicionado ao /etc/hosts com IP $ip.${NC}\n"
    else
      printf "${YELLOW}⚠️  Domínio $d já existe no /etc/hosts.${NC}\n"
    fi
  done
}

# Obter o IP público da VPS
PUBLIC_IP=$(get_public_ip)

while true; do
  printf "${WHITE}💻 Você deseja informar um domínio? (s/n):${GRAY_LIGHT} "
  read -r inform_domain

  if [[ "$inform_domain" == "s" || "$inform_domain" == "n" ]]; então
    break
  else
    printf "${YELLOW}❕ Opção inválida. Por favor, escolha 's' para sim ou 'n' para não.${GRAY_LIGHT}\n"
  fi
done

if [[ "$inform_domain" == "s" ]]; então
  while true; do
    printf "${WHITE}💻 Insira o domínio para a API:${GRAY_LIGHT} "
    read -r DOMAIN

    if [ -z "$DOMAIN" ]; então
      printf "${YELLOW}❕ O domínio nao pode ser vazio. Deseja tentar novamente? (s/n) ${GRAY_LIGHT}\n"
      while true; do
        read -r response
        if [[ "$response" == "s" || "$response" == "n" ]]; então
          break
        else
          printf "${YELLOW}❕ Opção inválida. Por favor, escolha 's' para sim ou 'n' para não.${GRAY_LIGHT}\n"
        fi
      done

      if [[ "$response" == "n" ]]; então
        DOMAIN="${PUBLIC_IP//./-}.nip.io"
        printf "${YELLOW}⚠️  Usando domínio nip.io: $DOMAIN${NC}\n"
        break
      else
        continue
      fi
    fi

    if ! validate_domain "$DOMAIN"; então
      printf "${YELLOW}❕ O domínio $DOMAIN não aponta para o IP da VPS ou não existe. Deseja tentar novamente? (s/n) ${GRAY_LIGHT}\n"
      while true; do
        read -r response
        if [[ "$response" == "s" || "$response" == "n" ]]; então
          break
        else
          printf "${YELLOW}❕ Opção inválida. Por favor, escolha 's' para sim ou 'n' para não.${GRAY_LIGHT}\n"
        fi
      done

      if [[ "$response" == "n" ]]; então
        DOMAIN="${PUBLIC_IP//./-}.nip.io"
        printf "${YELLOW}⚠️  Usando domínio nip.io: $DOMAIN${NC}\n"
        break
      else
        continue
      fi
    fi

    break
  done
fi

if [[ "$inform_domain" == "n" ]]; então
  DOMAIN="${PUBLIC_IP//./-}.nip.io"
  printf "${YELLOW}⚠️  Usando domínio nip.io: $DOMAIN${NC}\n"
fi

# Atribuicao de valores as variaveis
APP_URL="${HTTP_PROTOCOL}://${DOMAIN}"
API_URL="${HTTP_PROTOCOL}://api.${DOMAIN}"
USERS_URL="${HTTP_PROTOCOL}://users.${DOMAIN}"

# Adicionar domínios ao /etc/hosts
add_to_hosts "$DOMAIN"

# Criar ClusterIssuer para produção
microk8s kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    email: seu-email@gmail.com
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-account-key
    solvers:
    - http01:
        ingress:
          class: public
EOF

# Criar Certificate
microk8s kubectl apply -f - <<EOF
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: ${DOMAIN}-tls
  namespace: default
spec:
  secretName: ${DOMAIN}-tls-secret
  issuerRef:
    name: letsencrypt
    kind: ClusterIssuer
  commonName: ${DOMAIN}
  dnsNames:
  - ${DOMAIN}
  - api.${DOMAIN}
  - users.${DOMAIN}
EOF

# Criar Ingress
cat <<EOF | microk8s kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
spec:
  ingressClassName: nginx
  tls:
  - hosts:
    - ${DOMAIN}
    - api.${DOMAIN}
    - users.${DOMAIN}
    secretName: ${DOMAIN}-tls-secret
  rules:
  - host: ${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: client-service-svc
            port:
              number: 3000
  - host: api.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: api-service-svc
            port:
              number: 3000
  - host: users.${DOMAIN}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: user-service-svc
            port:
              number: 3000
EOF

printf "${GREEN}✔️  Configuração concluída com sucesso.${NC}\n"

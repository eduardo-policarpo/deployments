#!/bin/bash

# Configuração inicial
POD_NAME="mongodb-cluster-0"
NAMESPACE="infra"
MONGODB_USER="root"
DATE=$(date +%F)
BACKUP_DIR="/tmp"
LOCAL_BACKUP_DIR=$(pwd)
MONGODB_ROOT_PASSWORD=$(microk8s kubectl get secret --namespace infra mongodb-cluster -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)

# Solicitação de ação
echo "Este script pode fazer tanto backup quanto restore do MongoDB."
echo "Digite 1 para Backup ou 2 para Restore:"
read -r action

if [[ $action == "1" ]]; then
  # Execução do backup no MongoDB
  echo "Iniciando o backup do MongoDB..."
  sudo microk8s kubectl exec $POD_NAME -n $NAMESPACE -- mongodump --archive=$BACKUP_DIR/mongodb-backup-$DATE.gz --authenticationDatabase=admin -u $MONGODB_USER -p $MONGODB_ROOT_PASSWORD

  # Verificação do sucesso do backup e cópia para o diretório local
  if [ $? -eq 0 ]; then
    echo "Backup criado com sucesso. Copiando para o diretório local..."
    sudo microk8s kubectl cp $NAMESPACE/$POD_NAME:$BACKUP_DIR/mongodb-backup-$DATE.gz $LOCAL_BACKUP_DIR/mongodb-backup-$DATE.gz
    if [ $? -eq 0 ]; then
      echo "Backup copiado com sucesso para o diretório local."
    else
      echo "Erro ao copiar o backup para o diretório local."
    fi
  else
    echo "Falha ao criar o backup."
  fi
elif [[ $action == "2" ]]; then
  # Solicitação do arquivo de backup para restorezz
  echo "Por favor, forneça o caminho completo para o arquivo de backup para restore:"
  read -r backup_path

  # Verificação se o arquivo existe
  if [[ -f "$backup_path" ]]; then
    # Copia o arquivo de backup para o diretório temporário no pod
    echo "Copiando o arquivo de backup para o pod..."
    sudo microk8s kubectl cp $backup_path $NAMESPACE/$POD_NAME:$BACKUP_DIR/mongodb-restore.gz

    # Executando o restore
    echo "Iniciando o restore do MongoDB..."
    sudo microk8s kubectl exec $POD_NAME -n $NAMESPACE -- mongorestore --drop --archive=$BACKUP_DIR/mongodb-restore.gz --authenticationDatabase=admin -u $MONGODB_USER -p $MONGODB_ROOT_PASSWORD

    if [ $? -eq 0 ]; then
      echo "Restore concluído com sucesso."
    else
      echo "Falha ao restaurar o backup."
    fi
  else
    echo "Arquivo de backup não encontrado. Verifique o caminho fornecido e tente novamente."
  fi
else
  echo "Opção inválida. Digite 1 para Backup ou 2 para Restore."
fi

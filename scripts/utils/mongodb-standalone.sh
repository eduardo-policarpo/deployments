microk8s kubectl apply -f - <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongodb
  namespace: infra
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
      - name: mongodb
        image: mongo:4.4
        ports:
        - containerPort: 27017
        env:
        - name: MONGO_INITDB_ROOT_USERNAME
          value: "admin"
        - name: MONGO_INITDB_ROOT_PASSWORD
          value: "admin123"
---

apiVersion: v1
kind: Service
metadata:
  name: mongodb-service
  namespace: infra
spec:
  ports:
  - port: 27017
    targetPort: 27017
  selector:
    app: mongodb
---

apiVersion: v1
kind: Service
metadata:
  name: mongodb-external
  namespace: infra
spec:
  type: NodePort
  ports:
  - port: 27017
    targetPort: 27017
    protocol: TCP
    nodePort: 30676
  selector:
    app: mongodb
---
apiVersion: batch/v1
kind: Job
metadata:
  name: mongodb-setup
  namespace: infra
spec:
  template:
    spec:
      containers:
      - name: mongodb-setup
        image: mongo:4.4
        command: ["/bin/bash"]
        args:
        - "-c"
        - |
          mongo --host mongodb-service.infra.svc.cluster.local --authenticationDatabase admin -u admin -p admin123 <<EOF
          use wappi;

          db.createUser({
            user: "admin",
            pwd: "admin123",
            roles: [
              { role: "dbOwner", db: "wappi" },
              { role: "readWrite", db: "wappi" }
            ]
          });

          db.plans.insertMany([
            {
              "name": "EXPERIMENTAL",
              "maxInstances": 1,
              "price": 0,
              "expiresIn": 2,
              "isActive": true,
              "description": "Este plano permite testar a API por 2 dias.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "MENSAL",
              "maxInstances": 5,
              "price": 350,
              "expiresIn": 30,
              "isActive": true,
              "description": "Este plano oferece até 5 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "TRIMESTRAL",
              "maxInstances": 10,
              "price": 800,
              "expiresIn": 90,
              "isActive": true,
              "description": "Este plano oferece até 10 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "SEMESTRAL",
              "maxInstances": 30,
              "price": 1200,
              "expiresIn": 180,
              "isActive": true,
              "description": "Este plano oferece até 30 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            },
            {
              "name": "ANUAL",
              "maxInstances": 60,
              "price": 2500,
              "expiresIn": 365,
              "isActive": true,
              "description": "Este plano oferece até 60 instâncias com suporte 24/7.",
              "features": ["Suporte 24/7", "Backup diário"]
            }
          ]);

          // Aguardar a propagação dos dados antes de consultar
          sleep(5000);

          var planId = db.plans.findOne({name: "ANUAL"})._id;
          db.users.insertOne({
            "name": "System User",
            "phone": "4333754246",
            "email": "eduardo@gmail.com",
            "username": "system",
            "password": "@e50061FF461914D92B29556E31B53F510B60F8F1AFC4C25F87497C06996F99C720B9D163CD5729B262B5380B7827F153BFDB05710ECF08060755B40F1CDFE7EE4",
            "isActive": true,
            "planId": planId,
            "instances": [],
            "expiresIn": new Date("2099-02-28T13:13:44.675Z"),
            "isAdmin": true
          });
          EOF
        env:
        - name: MONGO_INITDB_ROOT_USERNAME
          value: "admin"
        - name: MONGO_INITDB_ROOT_PASSWORD
          value: "admin123"
      restartPolicy: OnFailure

EOF
